package com.mysphera.paido.service

import com.mysphera.paido.BadRequestException
import com.mysphera.paido.MicroServicesToolbox
import com.mysphera.paido.data.ChallengeCompletionRepository
import org.bson.types.ObjectId
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
internal class CompletionServiceTestValidateQuestionnaire {

    @Autowired
    private lateinit var completionService: CompletionService

    data class QuestionnaireWithAnswers(val questionnaire: String, val answers: String)

    val UD3_FINAL = QuestionnaireWithAnswers(
        """{"pages":[{"name":"page1","elements":[{"type":"radiogroup","name":"¿Cómo podemos diferenciar el pescado azul del blanco?","choices":["Por la forma de la cola","Por la cantidad de grasa","La respuesta A y la B son verdaderas"]},{"type":"radiogroup","name":"¿Por qué se considera que el pescado azul es más saludable en general que el blanco?:","choices":["Porque tiene más proteínas","Porque engorda menos","Porque a pesar de ser más grasos, las grasas que aportan son muy buenas para la salud"]},{"type":"radiogroup","name":"¿En cuanto al contenido de mercurio de los peces, cuál de estas afirmaciones te parece verdadera?","choices":["Los peces de pequeño tamaño son los que tiene más mercurio","Dentro de los peces con poco mercurio están el atún rojo, el emperador o pez espada y el tiburón","No tenemos que comer más de una lata de atún a la semana"]}]}]}""",
        """{"¿Cómo podemos diferenciar el pescado azul del blanco?":"La respuesta A y la B son verdaderas","¿Por qué se considera que el pescado azul es más saludable en general que el blanco?:":"Porque a pesar de ser más grasos, las grasas que aportan son muy buenas para la salud","¿En cuanto al contenido de mercurio de los peces, cuál de estas afirmaciones te parece verdadera?":"No tenemos que comer más de una lata de atún a la semana"}"""
    )

    val FRUTAS_TEMPORADA = QuestionnaireWithAnswers(
        """{"pages":[{"name":"page1","elements":[{"type":"checkbox","name":"Quantes peces de fruita meges habitualment al dia?","isRequired":true,"choices":["1","2","3 ó més"]},{"type":"checkbox","name":"En quin moment del dia menges fruita?","description":"Menjar fruita ","isRequired":true,"choices":["Esmorzar","Dinar","Sopar"]},{"type":"text","name":"Quina es la teua fruita preferida?","description":"Opinió personal","isRequired":true},{"type":"boolean","name":"Consideres que és important menjar fruita?","description":"Importància d'incloure la fruita com aliment.","isRequired":"false","labelTrue":"Sí","labelFalse":"No"}]}]}""",
        """{"Quantes peces de fruita meges habitualment al dia?":["3 ó més"],"En quin moment del dia menges fruita?":["Esmorzar","Dinar","Sopar"],"Quina es la teua fruita preferida?":"pera","Consideres que és important menjar fruita?":true}"""
    )

    @Test
    fun `Fails for empty answer if questions are required`() {
        val id = ObjectId()
        assertThrows(
            BadRequestException::class.java
        ) {
            completionService.validateQuestionnaire(
                id,
                FRUTAS_TEMPORADA.questionnaire,
                FRUTAS_TEMPORADA.answers,
                "{}"
            )
        }
    }

    @Test
    fun `Accepts empty answer if questions are not required`() {
        val id = ObjectId()
        assertEquals(
            true,
            completionService.validateQuestionnaire(
                id,
                UD3_FINAL.questionnaire,
                UD3_FINAL.answers,
                "{}"
            )
        )
    }
}