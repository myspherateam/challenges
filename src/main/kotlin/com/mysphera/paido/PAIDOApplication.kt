package com.mysphera.paido

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@SpringBootApplication
@EnableScheduling
class PAIDOApplication : WebMvcConfigurer {
    @Autowired
    private lateinit var authInterceptor: PAIDOInterceptor

    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(authInterceptor).addPathPatterns(
            mutableListOf("/challenge**/**", "/didacticUnit**/**", "/badge**/**", "/admin**/**", "/notification**/**")
        )
    }
}

fun main(args: Array<String>) {
    SpringApplication.run(PAIDOApplication::class.java, *args)
}