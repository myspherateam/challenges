package com.mysphera.paido

import com.mysphera.paido.data.ChallengeCompletion
import com.mysphera.paido.data.ChallengeType
import org.bson.types.ObjectId

data class JWTPayload(
    val id: ObjectId,
    val originalId: ObjectId,
    val caringFor: List<ObjectId>
)

data class Response<T>(
    var message: T,
    var status: Int = 200,
    val statusMessage: String? = "OK",
    var count: Number? = null,
    var errorCode: String? = null
)

data class PaginatedData<T>(
    var data: T,
    var total: Number
)

@Suppress("unused")
enum class Rol(
    val type: String
) {
    CHALLENGE_EDITOR("ChallengeEditor"),
    CHILD("Child"),
    PARENT("Parent"),
    TEACHER("Teacher"),
    CLINICIAN("Clinician"),
    LOCAL_ADMIN("LocalAdmin"),
    GLOBAL_ADMIN("GlobalAdmin");

    override fun toString(): String {
        return this.type
    }
}

data class NumChallenges(
    val physical: Int,
    val authority: Int,
    val gps: Int,
    val questionnaire: Int,
    val news: Int
) {
    fun plus(other: NumChallenges?): NumChallenges {
        if (other == null) {
            return this
        }
        return NumChallenges(
            physical + other.physical,
            authority + other.authority,
            gps + other.gps,
            questionnaire + other.questionnaire,
            news + other.news
        )
    }

    fun plus(challenge: ChallengeCompletion): NumChallenges {
        return NumChallenges(
            physical + if (challenge.type == ChallengeType.PHYSICAL.type) 1 else 0,
            authority + if (challenge.type == ChallengeType.AUTHORITY.type) 1 else 0,
            gps + if (challenge.type == ChallengeType.GPS.type) 1 else 0,
            questionnaire + if (challenge.type == ChallengeType.QUESTIONNAIRE.type) 1 else 0,
            news + if (challenge.type == ChallengeType.NEWS.type) 1 else 0
        )
    }

    companion object {
        val empty = NumChallenges(0, 0, 0, 0, 0)

        fun fromChallenges(
            challenges: List<ChallengeCompletion>,
            countChallenge: (challenge: ChallengeCompletion) -> Boolean
        ): NumChallenges {
            return challenges.fold(empty) { acc, challengeCompletion ->
                if (countChallenge(challengeCompletion)) {
                    acc.plus(challengeCompletion)
                } else {
                    acc
                }
            }
        }
    }
}

@Suppress("unused")
open class User(
    var id: ObjectId? = null,
    var roles: List<String>? = null,
    var potentialPoints: Int? = null,
    var points: Int? = null,
    var steps: Int? = null,
    var numAssignedChallenges: NumChallenges? = null,
    var numCompletedChallenges: NumChallenges? = null,
    var externalId: String? = null,
    var displayId: Int? = null,
    var displayName: String? = null,
    var entityIds: List<ObjectId>? = null,
    var groupIds: List<ObjectId>? = null,
    var caringForIds: List<ObjectId>? = null,
    var parents: List<ObjectId>? = null,
    var password: String? = null,
    var badges: List<ObjectId>? = null,
    var futureBadges: List<ObjectId>? = null
) {
    fun resolveUserId(): ObjectId {
        return this.id ?: throw DataException("User has no id")
    }

    fun hasRol(rol: Rol): Boolean {
        return this.roles?.contains(rol.toString()) ?: false
    }

    fun isParentOf(id: ObjectId): Boolean {
        return if (this.hasRol(Rol.PARENT)) {
            this.caringForIds?.contains(id) ?: false
        } else {
            false
        }
    }

    fun resolveDescription(): String {
        val displayId = this.displayId
        val displayName = this.displayName
        val externalId = this.externalId

        return when {
            displayName == null && externalId == null -> {
                displayId.toString()
            }
            displayName == null && !externalId.isNullOrEmpty() -> {
                String.format("%d-%s", displayId, externalId)
            }
            else -> {
                displayName ?: displayId.toString()
                ?: throw DataException("user has no displayId ${this.id}")
            }
        }
    }
}

data class ChallengeSubcategoryCount(
    var challenges: Int = 0,
    var news: Int = 0,
    var questionnaires: Int = 0,
    var activities: Int = 0
)

data class ChallengeCategoryCount(
    var count: Int = 0,
    var subcategories: ChallengeSubcategoryCount = ChallengeSubcategoryCount()
)

data class ChallengeCount(
    var prepare: ChallengeCategoryCount = ChallengeCategoryCount(),
    var activate: ChallengeCategoryCount = ChallengeCategoryCount(),
    var avoid: ChallengeCategoryCount = ChallengeCategoryCount(),
    var enjoy: ChallengeCategoryCount = ChallengeCategoryCount(),
    var keep: ChallengeCategoryCount = ChallengeCategoryCount()
)