package com.mysphera.paido

abstract class ValueException(val value: Any) : RuntimeException(value.toString())
class MissingAuthenticationException(value: Any? = null) :
    ValueException(value?.toString() ?: "MissingAuthenticationException")

class ForbiddenException(value: Any? = null) :
    ValueException(value?.toString() ?: "ForbiddenException")

class BadRequestException(value: Any? = null) :
    ValueException(value?.toString() ?: "BadRequestException")

class NotFoundException(value: Any? = null) :
    ValueException(value?.toString() ?: "NotFoundException")

class DataException(value: Any? = null) : ValueException(value?.toString() ?: "DataException")
class ServerException(value: Any? = null) : ValueException(value?.toString() ?: "ServerException")