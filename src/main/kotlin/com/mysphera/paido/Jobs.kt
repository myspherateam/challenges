package com.mysphera.paido

import com.mysphera.paido.service.ProgrammingService
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Suppress("unused")
@Component
class ChallengeProgrammingScheduler {
    private val logger = KotlinLogging.logger {}

    @Value("\${challenge-periods-limit}")
    private lateinit var periodsLimit: String

    @Autowired
    private lateinit var programmingService: ProgrammingService

    @Scheduled(cron = "\${challenge-programming-scheduling.next-period-completions.cron}")
    fun nextPeriodCompletions() {
        logger.info { "Starting 'challenge-programming-scheduling.next-period-completions' job" }
        val createdCount = programmingService.createNextCompletions(periodsLimit.toInt())
        logger.info { "ChallengeCompletion created: $createdCount" }
    }
}
