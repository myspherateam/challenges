package com.mysphera.paido

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.http.client.ClientHttpResponse
import org.springframework.http.converter.FormHttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.client.DefaultResponseErrorHandler
import org.springframework.web.client.RestTemplate

@Suppress("unused")
@Configuration
class RestTemplateConfig {
    @Autowired
    private lateinit var mappingJackson2HttpMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var formHttpMessageConverter: FormHttpMessageConverter

    @Bean
    fun customRestTemplate(): RestTemplate {
        val http =
            RestTemplate(listOf(mappingJackson2HttpMessageConverter, formHttpMessageConverter))
        http.errorHandler = CustomErrorHandler()
        return http
    }
}

private class CustomErrorHandler : DefaultResponseErrorHandler() {
    /**
     * Avoid default Spring's Rest Template behavior (Throw exceptions when it gets 400-500 status codes)
     */
    override fun hasError(response: ClientHttpResponse): Boolean {
        return false
    }
}

private fun buildRequest(
    token: String,
    body: Any? = null,
    headers: Map<String, String>?
): HttpEntity<Any?> {
    val httpHeaders = HttpHeaders()

    //Fixed headers
    httpHeaders.add(HttpHeaders.AUTHORIZATION, token)
    httpHeaders.add(HttpHeaders.USER_AGENT, "challenges")
    //Provided headers
    headers?.forEach { header ->
        httpHeaders.add(header.key, header.value)
    }

    return HttpEntity(body, httpHeaders)
}

fun RestTemplate.doPOST(
    token: String,
    url: String,
    body: Any,
    headers: Map<String, String>? = null
): ResponseEntity<Response<*>> {
    val httpEntity = buildRequest(token, body, headers)

    return this.exchange(url, HttpMethod.POST, httpEntity, Response::class.java)
}

fun RestTemplate.doGET(
    token: String,
    url: String,
    headers: Map<String, String>? = null
): ResponseEntity<Response<*>> {
    val httpEntity = buildRequest(token, headers = headers)

    return this.exchange(url, HttpMethod.GET, httpEntity, Response::class.java)
}