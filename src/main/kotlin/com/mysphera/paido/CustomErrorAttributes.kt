package com.mysphera.paido

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes
import org.springframework.stereotype.Component
import org.springframework.web.context.request.WebRequest

@Suppress("unused")
@Component
class CustomErrorAttributes : DefaultErrorAttributes() {
    @Autowired
    private lateinit var objectMapper: ObjectMapper

    override fun getErrorAttributes(
        webRequest: WebRequest?,
        includeStackTrace: Boolean
    ): MutableMap<String, Any> {
        val errors = super.getErrorAttributes(webRequest, includeStackTrace)
        val status = errors["status"] as Int
        val error = errors["error"] as String
        val message = errors["message"]

        val response = Response(message, status, error)

        return objectMapper.convertValue(response, mutableMapOf<String, Any>()::class.java)
    }
}