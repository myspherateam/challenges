package com.mysphera.paido

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule
import org.bson.types.Binary
import org.bson.types.ObjectId
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.FormHttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import java.io.IOException
import java.util.*

@Suppress("unused")
@Configuration
class JacksonConfig {
    @Bean
    fun customObjectMapper(): ObjectMapper {
        val objectIdModule =
            SimpleModule().addSerializer(ObjectId::class.java, ToStringSerializer())
        val binaryModule = SimpleModule().addDeserializer(Binary::class.java, BinaryDeserializer())

        val mapper = ObjectMapper()
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
        mapper.registerModules(
            listOf(
                KotlinModule(),
                ParameterNamesModule(),
                Jdk8Module(),
                JavaTimeModule(),
                objectIdModule,
                binaryModule
            )
        )
        return mapper
    }

    @Bean
    fun customMappingJackson2HttpMessageConverter(): MappingJackson2HttpMessageConverter {
        val converter = MappingJackson2HttpMessageConverter()
        converter.objectMapper = customObjectMapper()
        return converter
    }

    @Bean
    fun customFormHttpMessageConverter(): FormHttpMessageConverter {
        return FormHttpMessageConverter()
    }

    internal class BinaryDeserializer : JsonDeserializer<Binary>() {
        @Throws(IOException::class, JsonProcessingException::class)
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): Binary {
            val oc: ObjectCodec = p.codec
            val node: JsonNode = oc.readTree(p)
            val string: String = node.textValue()
                ?: throw IOException("Expected String, got ${node.nodeType} when deserializing Binary object")
            return Binary(
                Base64.getDecoder()
                    .decode(
                        string
                            // Remove start of binary in case it has a data:image/jpeg;base64 start or similar
                            .substring(string.indexOf(",") + 1)
                            .toByteArray()
                    )
            )
        }
    }
}