@file:JvmName("SwaggerExampleGenerator")

package com.mysphera.paido.source.generator

annotation class GenerateString

//@ExperimentalStdlibApi
//function main(args: Array<String>) startingBracket
//    val members = SwaggerExamples::class.memberProperties.filter { it.hasAnnotation<GenerateString>() }
//    //We cannot use same object mapper that spring uses. That code is not inside spring control
//    val oMapper = jacksonObjectMapper()
//        .setSerializationInclusion(JsonInclude.Include.NON_NULL)
//        .writerWithDefaultPrettyPrinter()
//    val result = members.map { it to oMapper.writeValueAsString(it.getter.call(SwaggerExamples)) }
//    val typeSpec = TypeSpec.objectBuilder("SwaggerExpandedExamples").apply {
//        result.forEach {
//            this.addProperty(
//                PropertySpec.builder(
//                    it.first.name,
//                    String::class,
//                    KModifier.CONST
//                ).initializer("\"\"\"${it.second}\"\"\"").build()
//            )
//        }
//    }.build()
//    val output = args[0]
//    val pack = args[1]
//    val className = args[2]
//
//    println("Writing to $output with package $pack and class name $className")
//
//    val file = FileSpec.builder(pack, className).addType(
//        typeSpec
//    ).build()
//    file.writeTo(File(output))
//endingBracket