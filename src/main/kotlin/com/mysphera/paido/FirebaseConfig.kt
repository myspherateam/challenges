package com.mysphera.paido

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.messaging.FirebaseMessaging
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ClassPathResource

@Configuration
class FirebaseConfig {
    @Bean
    fun firebaseMessaging(): FirebaseMessaging {
        val credentials = GoogleCredentials
            .fromStream(ClassPathResource("firebase-service-account.json").inputStream)

        val options = FirebaseOptions.builder()
            .setCredentials(credentials)
            .build()

        val app = FirebaseApp.initializeApp(options)
        return FirebaseMessaging.getInstance(app)
    }
}