package com.mysphera.paido.data

import com.mysphera.paido.User

object BadgeConfiguration {
    object PointsBadge : BadgeRequirements() {
        override fun nextRequirement(user: User): BadgeInfo? {
            return pointsBadges.find { it.requirement >= (user.points ?: 0) }
        }

        override fun awardedBadges(user: User): List<BadgeInfo> {
            return pointsBadges.filter { it.requirement < (user.points ?: 0) }
        }
    }
}

val pointsBadges = listOf(
    BadgeInfo("PointsBadge1", 100),
    BadgeInfo("PointsBadge2", 200),
    BadgeInfo("PointsBadge3", 500),
    BadgeInfo("PointsBadge4", 1000),
    BadgeInfo("PointsBadge5", 1500),
    BadgeInfo("PointsBadge6", 2000),
    BadgeInfo("PointsBadge7", 2500)
)

data class BadgeInfo(val code: String, val requirement: Int)

abstract class BadgeRequirements {
    abstract fun nextRequirement(user: User): BadgeInfo?
    abstract fun awardedBadges(user: User): List<BadgeInfo>
}