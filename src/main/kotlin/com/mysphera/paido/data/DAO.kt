package com.mysphera.paido.data

import com.mysphera.paido.*
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.aggregation.Aggregation
import org.springframework.data.mongodb.core.query.*
import org.springframework.stereotype.Component
import java.util.*

interface ChallengeProgrammingDAO {
    fun findWithOptionalFilters(
        pageable: Pageable? = null,
        entities: List<ObjectId>? = null,
        groups: List<ObjectId>? = null,
        users: List<ObjectId>? = null,
        expired: Boolean? = null
    ): PaginatedData<List<ChallengeProgramming>>

    fun findPeriodicAndNotExpired(): List<ChallengeProgramming>
}

@Suppress("unused")
@Component
class ChallengeProgrammingDAOImpl : ChallengeProgrammingDAO {
    @Autowired
    private lateinit var database: MongoTemplate

    override fun findWithOptionalFilters(
        pageable: Pageable?,
        entities: List<ObjectId>?,
        groups: List<ObjectId>?,
        users: List<ObjectId>?,
        expired: Boolean?
    ): PaginatedData<List<ChallengeProgramming>> {
        val query = Query().with(Sort.by(Sort.Direction.DESC, "finishDatetime"))

        if (entities != null) {
            query.addCriteria(Criteria.where("assignedToIds.entities").inValues(entities))
        }
        if (groups != null) {
            query.addCriteria(Criteria.where("assignedToIds.groups").inValues(groups))
        }
        if (users != null) {
            query.addCriteria(Criteria.where("assignedToIds.users").inValues(users))
        }
        when {
            (expired != null) && expired -> {
                query.addCriteria(Criteria.where("finishDatetime").lt(Date()))
            }
            (expired != null) && !expired -> {
                query.addCriteria(Criteria.where("finishDatetime").gte(Date()))
            }
        }

        val total = database.count(query, ChallengeProgramming::class.java)
        if (pageable != null) {
            query.skip(pageable.pageNumber.toLong() * pageable.pageSize.toLong())
            query.limit(pageable.pageSize)
        }

        val data = database.find(query, ChallengeProgramming::class.java)

        return PaginatedData(data, total)
    }

    override fun findPeriodicAndNotExpired(): List<ChallengeProgramming> {
        val query = Query()
            .addCriteria(Criteria.where("periodDays").ne(null))
            .addCriteria(Criteria.where("finishDatetime").gt(Date()))
        return database.find(query, ChallengeProgramming::class.java)
    }
}

interface ChallengeCompletionDAO {
    fun findWithOptionalFilters(
        childList: List<ObjectId>? = null,
        programmingId: ObjectId? = null,
        parent: ObjectId? = null,
        category: String? = null,
        subcategory: String? = null,
        type: String? = null,
        started: Boolean? = null,
        finished: Boolean? = null,
        completed: Boolean? = null,
        inverted: Boolean? = null,
        pageable: Pageable? = null,
        visible: Boolean? = null,
        sortFields: List<String>? = null,
        sortOrder: Sort.Direction? = null,
        limit: Int? = null
    ): PaginatedData<List<ChallengeCompletion>>

    fun findByProgrammingIdAndForIdAndCompleted(
        programmingId: ObjectId? = null,
        forId: ObjectId? = null,
        completed: Boolean? = null
    ): List<ChallengeCompletion>

    fun existsByForIdAndProgrammingId(forId: ObjectId, programmingId: ObjectId): Boolean

    fun findGroupedByCategoriesAndSubcategories(
        forIds: List<ObjectId>,
        parent: ObjectId?,
        started: Boolean?,
        finished: Boolean?
    ): ChallengeCount
}

@Suppress("unused")
@Component
class ChallengeCompletionDAOImpl : ChallengeCompletionDAO {
    @Autowired
    private lateinit var database: MongoTemplate

    /**
     * Note: 'limit' wont be used if 'pageable' has value (pagination priority)
     * Note: Sorting feature requires pass value for 'sortFields' and 'sortOrder' params together
     */
    override fun findWithOptionalFilters(
        childList: List<ObjectId>?,
        programmingId: ObjectId?,
        parent: ObjectId?,
        category: String?,
        subcategory: String?,
        type: String?,
        started: Boolean?,
        finished: Boolean?,
        completed: Boolean?,
        inverted: Boolean?,
        pageable: Pageable?,
        visible: Boolean?,
        sortFields: List<String>?,
        sortOrder: Sort.Direction?,
        limit: Int?
    ): PaginatedData<List<ChallengeCompletion>> {
        val criteriaList = mutableListOf<Criteria>(Criteria.where("deleted").ne(true))
        when (visible) {
            true, null -> criteriaList.add(Criteria.where("visibleDatetime").lt(Date()))
        }


        //TODO ia: Challenge was not override
        if (childList != null) {
            criteriaList.add(Criteria.where("forId").inValues(childList))
        }
        if (programmingId != null) {
            criteriaList.add(Criteria.where("programmingId").isEqualTo(programmingId))
        }
        if (category != null) {
            criteriaList.add(Criteria.where("category").isEqualTo(category))
        }
        if (subcategory != null) {
            criteriaList.add(Criteria.where("subcategory").isEqualTo(subcategory))
        }
        if (type != null) {
            criteriaList.add(Criteria.where("type").isEqualTo(type))
        }
        when (started) {
            true -> criteriaList.add(Criteria.where("periodStartDatetime").lt(Date()))
            false -> criteriaList.add(Criteria.where("periodStartDatetime").gt(Date()))
        }
        if (finished == true) {
            criteriaList.add(Criteria.where("periodFinishDatetime").lt(Date()))
        } else if (finished == false) {
            criteriaList.add(Criteria.where("periodFinishDatetime").gt(Date()))
        }
        if (completed == true) {
            criteriaList.add(
                Criteria().orOperator(
                    Criteria().andOperator(
                        Criteria.where("assignedFor").inValues(listOf("child", "parent")),
                        Criteria.where("completed").isEqualTo(true),
                        Criteria.where("completedByParents").not().size(0)
                    ),
                    Criteria().andOperator(
                        Criteria.where("assignedFor").inValues("child"),
                        Criteria.where("completed").isEqualTo(true)
                    ),
                    Criteria().andOperator(
                        Criteria.where("assignedFor").inValues("parent"),
                        Criteria.where("completedByParents").not().size(0)
                    )
                )
            )
        } else if (completed == false) {
            if (inverted == true) {
                if (parent != null) {
                    criteriaList.add(
                        Criteria().andOperator(
                            Criteria.where("assignedFor").inValues("child"),
                            Criteria.where("completed").inValues(false, null)
                        )
                    )

                } else {
                    criteriaList.add(
                        Criteria().orOperator(
                            Criteria().andOperator(
                                Criteria.where("assignedFor").inValues("parent"),
                                Criteria.where("completedByParents")
                                    .isEqualTo(listOf<CompletedByParent>())
                            ),
                            Criteria().andOperator(
                                Criteria.where("assignedFor").inValues("child"),
                                Criteria.where("completed").inValues(false, null),
                                Criteria.where("type").isEqualTo(ChallengeType.AUTHORITY.type)
                            )
                        )

                    )
                }
            } else {
                if (parent != null) {
                    criteriaList.add(
                        Criteria().orOperator(
                            Criteria().andOperator(
                                Criteria.where("assignedFor").inValues("parent"),
                                Criteria.where("completedByParents.parentId").ne(parent)
                            ),
                            Criteria().andOperator(
                                Criteria.where("assignedFor").inValues("child"),
                                Criteria.where("completed").inValues(false, null),
                                Criteria.where("type").isEqualTo(ChallengeType.AUTHORITY.type)
                            )
                        )
                    )
                } else {
                    criteriaList.add(
                        Criteria().andOperator(
                            Criteria.where("assignedFor").inValues("child"),
                            Criteria.where("completed").inValues(false, null)
                        )
                    )
                }
            }
//            criteriaList.add(Criteria().orOperator(
//                Criteria().andOperator(
//                    Criteria.where("assignedFor").inValues("child"),
//                    Criteria.where("completed").inValues(false, null)
//                ),
//                Criteria().andOperator(
//                    Criteria.where("assignedFor").inValues("parent"),
//                    if (parent != null) {
//                        //Parent is asking for their own challenges (Parent's cant see each other challenges)
//                        Criteria.where("completedByParents.parentId").ne(parent)
//                    } else {
//                        //Child (or parent is name of child) is asking for parent's challenges
//                        Criteria.where("completedByParents").size(0)
//                    }
//                )
//            ))
        }

        val query = Query()
        query.addCriteria(Criteria().andOperator(*criteriaList.toTypedArray()))

        if (sortOrder != null && !sortFields.isNullOrEmpty()) {
            query.with(Sort.by(sortOrder, *sortFields.toTypedArray()))
        }

        if (limit != null) {
            query.limit(limit)
        }

        val total = database.count(query, ChallengeCompletion::class.java)
        if (pageable != null) {
            query.skip(pageable.pageNumber.toLong() * pageable.pageSize.toLong())
            query.limit(pageable.pageSize)
        }

        val data = database.find(query, ChallengeCompletion::class.java)

        return PaginatedData(data, total)
    }

    override fun findByProgrammingIdAndForIdAndCompleted(
        programmingId: ObjectId?,
        forId: ObjectId?,
        completed: Boolean?
    ): List<ChallengeCompletion> {
        val query = Query().addCriteria(Criteria.where("deleted").ne(true))

        if (programmingId != null) {
            query.addCriteria(Criteria.where("programmingId").isEqualTo(programmingId))
        }
        if (forId != null) {
            query.addCriteria(Criteria.where("forId").isEqualTo(forId))
        }
        if (completed == true) {
            query.addCriteria(
                Criteria().orOperator(
                    Criteria().andOperator(
                        Criteria.where("assignedFor").inValues(listOf("child", "parent")),
                        Criteria.where("completed").isEqualTo(true),
                        Criteria.where("completedByParents").not().size(0)
                    ),
                    Criteria().andOperator(
                        Criteria.where("assignedFor").inValues("child"),
                        Criteria.where("completed").isEqualTo(true)
                    ),
                    Criteria().andOperator(
                        Criteria.where("assignedFor").inValues("parent"),
                        Criteria.where("completedByParents").not().size(0)
                    )
                )
            )
        }

        return database.find(query, ChallengeCompletion::class.java)
    }

    override fun existsByForIdAndProgrammingId(forId: ObjectId, programmingId: ObjectId): Boolean {
        val query = Query()
        query.addCriteria(Criteria.where("forId").isEqualTo(forId))
        query.addCriteria(Criteria.where("programmingId").isEqualTo(programmingId))
        val result = database.find(query, ChallengeCompletion::class.java)
        return result.isNotEmpty()
    }

    override fun findGroupedByCategoriesAndSubcategories(
        forIds: List<ObjectId>,
        parent: ObjectId?,
        started: Boolean?,
        finished: Boolean?
    ): ChallengeCount {
        val isStarted = started ?: true
        val isFinished = finished ?: false

        val query = Aggregation.newAggregation(
            ChallengeCompletion::class.java,
            Aggregation.match(
                Criteria().andOperator(
                    Criteria.where("deleted").ne(true),
                    if (isStarted) {
                        Criteria.where("periodStartDatetime").lt(Date())
                    } else {
                        Criteria.where("periodStartDatetime").gt(Date())
                    },
                    if (isFinished) {
                        Criteria.where("periodFinishDatetime").lt(Date())
                    } else {
                        Criteria.where("periodFinishDatetime").gt(Date())
                    },
                    Criteria.where("category").ne(null),
                    Criteria.where("subcategory").ne(null),
                    Criteria.where("forId").inValues(forIds),
                    if (parent != null) {
                        Criteria().orOperator(
                            Criteria().andOperator(
                                Criteria.where("assignedFor").inValues("parent"),
                                Criteria.where("completedByParents.parentId").ne(parent)
                            ),
                            Criteria().andOperator(
                                Criteria.where("assignedFor").inValues("child"),
                                Criteria.where("completed").inValues(false, null),
                                Criteria.where("type").isEqualTo("authority"),
                                Criteria.where("childRelation").inValues("parent")
                            )
                        )
                    } else {
                        Criteria().andOperator(
                            Criteria.where("assignedFor").inValues("child"),
                            Criteria.where("completed").inValues(false, null)
                        )
                    }
                )
            ),
            Aggregation.group("category", "subcategory").count().`as`("total")
        )

        val resultSet = database.aggregate(query, Any::class.java)

        val result = ChallengeCount()

        val categoryKey = "category"
        val subcategoryKey = "subcategory"
        val totalKey = "total"
        resultSet.forEach { row ->
            val resultSetMap = row as LinkedHashMap<*, *>

            val challengeCategory = when (resultSetMap[categoryKey]) {
                ChallengeCategory.AVOID.type -> {
                    result.avoid
                }
                ChallengeCategory.PREPARE.type -> {
                    result.prepare
                }
                ChallengeCategory.ENJOY.type -> {
                    result.enjoy
                }
                ChallengeCategory.KEEP.type -> {
                    result.keep
                }
                ChallengeCategory.ACTIVATE.type -> {
                    result.activate
                }
                else -> {
                    ChallengeCategoryCount()
                }
            }

            val total = resultSetMap[totalKey] as Int

            challengeCategory.count += total
            when (resultSetMap[subcategoryKey]) {
                ChallengeSubcategory.ACTIVITIES.type -> {
                    challengeCategory.subcategories.activities += total
                }
                ChallengeSubcategory.QUESTIONNAIRES.type -> {
                    challengeCategory.subcategories.questionnaires += total
                }
                ChallengeSubcategory.NEWS.type -> {
                    challengeCategory.subcategories.news += total
                }
                ChallengeSubcategory.CHALLENGES.type -> {
                    challengeCategory.subcategories.challenges += total
                }
            }
        }

        return result
    }
}

interface ChallengeDefinitionDAO {
    fun findWithOptionalFilters(
        pageable: Pageable?,
        category: String?,
        subcategory: String?,
        type: String?
    ): PaginatedData<List<ChallengeDefinition>>

    fun findDefaultPoints(
        assignedFor: String?
    ): List<ChallengeDefaultPoints>

    fun addDidacticUnit(didacticUnitId: ObjectId, challengeIds: List<ObjectId>)

    fun removeDidacticUnit(didacticUnitId: ObjectId, challengeIds: List<ObjectId>)
}

@Suppress("unused")
@Component
class ChallengeDefinitionDAOImpl : ChallengeDefinitionDAO {
    @Autowired
    private lateinit var database: MongoTemplate

    override fun findWithOptionalFilters(
        pageable: Pageable?,
        category: String?,
        subcategory: String?,
        type: String?
    ): PaginatedData<List<ChallengeDefinition>> {
        val query = Query().addCriteria(Criteria.where("deleted").ne(true))

        if (category != null) {
            query.addCriteria(Criteria.where("category").isEqualTo(category))
        }
        if (subcategory != null) {
            query.addCriteria(Criteria.where("subcategory").isEqualTo(subcategory))
        }
        if (type != null) {
            query.addCriteria(Criteria.where("type").isEqualTo(type))
        }

        val total = database.count(query, ChallengeDefinition::class.java)
        if (pageable != null) {
            query.skip(pageable.pageNumber.toLong() * pageable.pageSize.toLong())
            query.limit(pageable.pageSize)
        }

        val data = database.find(query, ChallengeDefinition::class.java)

        return PaginatedData(data, total)
    }

    override fun findDefaultPoints(assignedFor: String?): List<ChallengeDefaultPoints> {
        val query = Query()

        if (assignedFor != null) {
            query.addCriteria(Criteria.where("assignedFor").isEqualTo(assignedFor))
        }

        return database.find(query, ChallengeDefaultPoints::class.java)
    }

    override fun addDidacticUnit(didacticUnitId: ObjectId, challengeIds: List<ObjectId>) {
        val query = Query().addCriteria(Criteria.where("_id").`in`(challengeIds))
        val update = Update().addToSet("didacticUnitsIds", didacticUnitId)
        val result = database.updateMulti(query, update, ChallengeDefinition::class.java)
        if (result.matchedCount != challengeIds.size.toLong()) {
            //TODO change to logging library warn
            println("Some of the challenges found not updated when adding didactic unit $didacticUnitId")
        }
    }

    override fun removeDidacticUnit(didacticUnitId: ObjectId, challengeIds: List<ObjectId>) {
        val query = Query().addCriteria(Criteria.where("_id").`in`(challengeIds))
        val update = Update().pull("didacticUnitsIds", didacticUnitId)
        val result = database.updateMulti(query, update, ChallengeDefinition::class.java)
        if (result.matchedCount != challengeIds.size.toLong()) {
            //TODO change to logging library warn
            println("Some of the challenges found not updated when adding didactic unit $didacticUnitId")
        }
    }
}

interface DidacticUnitDAO {
    fun findWithOptionalFilters(
        pageable: Pageable? = null,
        ids: List<ObjectId>? = null,
        name: String? = null
    ): PaginatedData<List<DidacticUnit>>
}

@Suppress("unused")
@Component
class DidacticUnitDAOImpl : DidacticUnitDAO {
    @Autowired
    private lateinit var database: MongoTemplate

    override fun findWithOptionalFilters(
        pageable: Pageable?,
        ids: List<ObjectId>?,
        name: String?
    ): PaginatedData<List<DidacticUnit>> {
        val query = Query().addCriteria(Criteria.where("deleted").ne(true))

        if (ids != null) {
            query.addCriteria(Criteria.where("_id").inValues(ids))
        }
        if (name != null) {
            query.addCriteria(Criteria.where("name").isEqualTo(name))
        }

        val total = database.count(query, DidacticUnit::class.java)
        if (pageable != null) {
            query.skip(pageable.pageNumber.toLong() * pageable.pageSize.toLong())
            query.limit(pageable.pageSize)
        }

        val data = database.find(query, DidacticUnit::class.java)

        return PaginatedData(data, total)
    }
}

interface BadgeDAO {
    fun findWithOptionalFilters(
        pageable: Pageable? = null,
        ids: List<ObjectId>? = null,
        name: String? = null,
        includePictures: Boolean?
    ): PaginatedData<List<Badge>>

    fun getBadgesFromUser(
        user: User,
        includePictures: Boolean?
    ): UserBadges

    fun findByCode(
        code: String
    ): Badge
}

@Suppress("unused")
@Component
class BadgeDAOImpl : BadgeDAO {
    @Autowired
    private lateinit var database: MongoTemplate

    override fun findWithOptionalFilters(
        pageable: Pageable?,
        ids: List<ObjectId>?,
        name: String?,
        includePictures: Boolean?
    ): PaginatedData<List<Badge>> {
        val query = Query().addCriteria(Criteria.where("deleted").ne(true))

        if (ids != null) {
            query.addCriteria(Criteria.where("_id").inValues(ids))
        }
        if (name != null) {
            query.addCriteria(Criteria.where("name").isEqualTo(name))
        }
        if (includePictures != true) {
            query.fields().exclude("image")
        }

        val total = database.count(query, Badge::class.java)
        if (pageable != null) {
            query.skip(pageable.pageNumber.toLong() * pageable.pageSize.toLong())
            query.limit(pageable.pageSize)
        }

        val data = database.find(query, Badge::class.java)

        return PaginatedData(data, total)
    }

    override fun getBadgesFromUser(
        user: User,
        includePictures: Boolean?
    ): UserBadges {
        val badgeIds = user.badges ?: listOf()
        val nextBadgeIds = user.futureBadges ?: listOf()
        val completedBadges =
            findWithOptionalFilters(null, badgeIds, includePictures = includePictures)
        val nextBadges =
            findWithOptionalFilters(null, nextBadgeIds, includePictures = includePictures)
        return UserBadges(completedBadges.data, nextBadges.data)
    }

    override fun findByCode(code: String): Badge {
        val query = Query().addCriteria(Criteria.where("code").isEqualTo(code))
        return database.find(query, Badge::class.java).firstOrNull() ?: throw NotFoundException()
    }
}

interface DeviceDAO {
     fun findByIdUser(
        id: ObjectId
    ): List<NotificationDevice>

    fun findByToken(
        token: String
    ): NotificationDevice?
}

@Suppress("unused")
@Component
class DeviceDAOImpl : DeviceDAO {
    @Autowired
    private lateinit var database: MongoTemplate

    override fun findByIdUser(id: ObjectId): List<NotificationDevice> {
        val query = Query().addCriteria(Criteria.where("userId").isEqualTo(id))
        return database.find(query, NotificationDevice::class.java)
    }

    override fun findByToken(token: String): NotificationDevice? {
        val query = Query().addCriteria(Criteria.where("token").isEqualTo(token))
        return database.findOne(query, NotificationDevice::class.java)
    }
}