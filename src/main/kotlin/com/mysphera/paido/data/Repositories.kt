package com.mysphera.paido.data

import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository

interface ChallengeDefinitionRepository : MongoRepository<ChallengeDefinition, ObjectId>,
    ChallengeDefinitionDAO

interface ChallengeProgrammingRepository : MongoRepository<ChallengeProgramming, ObjectId>,
    ChallengeProgrammingDAO

interface ChallengeCompletionRepository : MongoRepository<ChallengeCompletion, ObjectId>,
    ChallengeCompletionDAO

interface DidacticUnitRepository : MongoRepository<DidacticUnit, ObjectId>, DidacticUnitDAO

interface BadgeRepository : MongoRepository<Badge, ObjectId>, BadgeDAO
interface NotificationRepository : MongoRepository<NotificationDevice, ObjectId>, DeviceDAO
