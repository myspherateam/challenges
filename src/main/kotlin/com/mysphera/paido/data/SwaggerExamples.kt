package com.mysphera.paido.data

object SwaggerExamples {
    const val NEW_CHALLENGE_DEFINITION_NAME = "ChallengeDefinition"
    const val NEW_CHALLENGE_DEFINITION_DESC = "How to create different challenge definitions"
    const val NEW_CHALLENGE_DEFINITION = """
//PHYSICAL
{
    "title": "Reto 0",
    "description": "Descripcion",
    "category": "prepare | activate | avoid | enjoy | keep",
    "subcategory": "news | questionnaires | activities | challenges",
    "points": {
        "onlyChild": 599
    },
    "assignedFor": [
        "child"
    ],
    "type": "physical",
    "steps": 300
}
//GPS
{
    "title": "Reto 1",
    "description": "Descripcion",
    "category": "prepare | activate | avoid | enjoy | keep",
    "subcategory": "news | questionnaires | activities | challenges",
    "points": {
        "onlyChild": 599
    },
    "assignedFor": [
        "child"
    ],
    "type": "gps",
    "location": {
        "type": "Polygon",
        "coordinates": [
            [
                [
                    44.0,
                    55.0
                ]
            ]
        ]
    }
}
//AUTHORITY
{
    "title": "Reto 2",
    "description": "Descripcion",
    "category": "prepare | activate | avoid | enjoy | keep",
    "subcategory": "news | questionnaires | activities | challenges",
    "points": {
        "onlyChild": 599
    },
    "assignedFor": [
        "child"
    ],
    "type": "authority",
    "specificIds": [
        "5e4e5a31c94e43243022bc9c"
    ]
}
//QUESTIONNAIRE
{
    "title": "Reto 3",
    "description": "Descripcion",
    "category": "prepare | activate | avoid | enjoy | keep",
    "subcategory": "news | questionnaires | activities | challenges",
    "points": {
        "onlyChild": 599
    },
    "assignedFor": [
        "child"
    ],
    "type": "questionnaire",
    "questionnaire": "{}",
    "questionnaireCorrectAnswer": "{}"
}
//NEWS
{
    "title": "Reto 3",
    "description": "Descripcion",
    "category": "prepare | activate | avoid | enjoy | keep",
    "subcategory": "news | questionnaires | activities | challenges",
    "points": {
        "onlyChild": 599
    },
    "assignedFor": [
        "child"
    ],
    "type": "news",
    "news": "News content"
}
"""

    const val NEW_CHALLENGE_PROGRAMMING_NAME = "ChallengeProgramming"
    const val NEW_CHALLENGE_PROGRAMMING_DESC = "How to assign a challenge"
    const val NEW_CHALLENGE_PROGRAMMING = """
{
"definitionId": "5e4544501d6eaa3dc5117902",
"assignedToIds": {
    "entities": ["5e3812b3dd805c0a27371067"]
},
"finishDatetime": "2020-03-20T11:58:59Z",
"startDatetime": "2020-03-10T11:58:59Z",
}
"""

    const val NEW_CHALLENGE_PROGRAMMING_OVERRIDING_NAME = "ChallengeProgramming"
    const val NEW_CHALLENGE_PROGRAMMING_OVERRIDING_DESC = "How to assign overriding age values from definition"
    const val NEW_CHALLENGE_PROGRAMMING_OVERRIDING = """
{
"definitionId": "5e4544501d6eaa3dc5117902",
"assignedToIds": {
    "entities": ["5e3812b3dd805c0a27371067"]
},
"finishDatetime": "2020-03-20T11:58:59Z",
"startDatetime": "2020-03-10T11:58:59Z",
"minAge": 15,
"maxAge": 18
}
"""

    const val NEW_CHALLENGE_COMPLETION_NAME = "ChallengeCompletion"
    const val NEW_CHALLENGE_COMPLETION_DESC = "How to complete a challenge"
    const val NEW_CHALLENGE_COMPLETION = """
//PHYSICAL
{
    "id": "5e4e5a31c94e43243022bc9c",
    "userSteps": 5000
}
//GPS
{
    "id": "5e4e5a31c94e43243022bc9c",
    "precision": 55.3,
    "userLocation": {
        "coordinates": [53.3, 66.4]
    }
}
//AUTHORITY
{
    "id": "5e4e5a31c94e43243022bc9c",
}
//QUESTIONNAIRE
{
    "id": "5e4e5a31c94e43243022bc9c",
    "questionnaireAnswer": "{}"
    
}
//NEWS
{
    "id": "5e4e5a31c94e43243022bc9c",
    "newsAnswer": "{}"
}
"""

    const val RESPONSE_NAME = "Response"
    const val RESPONSE_DESC = "The request body inside a wrapper object"
    const val RESPONSE_EXAMPLE = """
{
    "message": {}
}
"""
}