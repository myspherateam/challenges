package com.mysphera.paido.data

import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Schema
import org.bson.types.Binary
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Transient
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

enum class ChildRelation(val type: String) {
    PARENT("parent"),
    TEACHER("teacher"),
    CLINICIAN("clinician")
}

enum class AssignedFor(val type: String) {
    CHILD("child"),
    PARENT("parent")
}

enum class ChallengeCategory(val type: String) {
    PREPARE("prepare"),
    ACTIVATE("activate"),
    AVOID("avoid"),
    ENJOY("enjoy"),
    KEEP("keep")
}

enum class ChallengeSubcategory(val type: String) {
    CHALLENGES("challenges"),
    ACTIVITIES("activities"),
    NEWS("news"),
    QUESTIONNAIRES("questionnaires")
}

enum class ChallengeType(val type: String) {
    PHYSICAL("physical"),
    AUTHORITY("authority"),
    GPS("gps"),
    QUESTIONNAIRE("questionnaire"),
    NEWS("news")
}

data class ChallengePoints(
    var onlyParent: Int? = null,
    var onlyChild: Int? = null,
    var both: Int? = null
)

data class ChallengeLocationPoint(
    var type: String? = "Point",
    var coordinates: Array<Double>? = null
)

data class ChallengeLocationPolygon(
    var type: String? = "Polygon",
    var coordinates: Array<Array<Array<Double>>>? = null
)

data class AssignedToIds(
    var entities: List<ObjectId>? = null,
    var groups: List<ObjectId>? = null,
    var users: List<ObjectId>? = null,
    var excludedUsersIds: List<ObjectId>? = null
)

data class CompletedByParent(
    var parentId: ObjectId? = null,
    var completionDatetime: Instant? = null
) {
    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other !is CompletedByParent) return false

        return this.parentId == other.parentId
    }

    override fun hashCode(): Int {
        return this.parentId.hashCode()
    }
}

data class DidacticUnitChallenge(
    @field:Schema(type = "string")
    var challengeId: ObjectId,
    var startOffset: Long,
    var endOffset: Long
)

@Document("DidacticUnit")
open class DidacticUnit(
    @field:Schema(type = "string")
    @Id var id: ObjectId? = null,
    var createdById: ObjectId? = null,
    var deletedById: ObjectId? = null,
    var name: String? = null,
    var deleted: Boolean? = null,
    var challenges: Array<DidacticUnitChallenge>? = null,
    var duration: Long? = null,
    @field:Schema(type = "string")
    var badgeId: ObjectId?
)

data class UserBadges(
    var badges: List<Badge>,
    var futureBadges: List<Badge>
)

@Document("NotificationDevice")
data class NotificationDevice(
    @field:Schema(type = "string")
    @Id var id: ObjectId? = null,
    @field:Schema(type = "string")
    var token: String,
    @field:Schema(type = "string")
    var userId: ObjectId
)

@Suppress("unused")
@Document("Badge")
open class Badge(
    @field:Schema(type = "string")
    @Id var id: ObjectId? = null,
    var createdById: ObjectId? = null,
    var name: String? = null,
    var description: String? = null,
    var image: Binary? = null,
    var code: String? = null,
    var currentProgress: Int? = null,
    var nextMilestone: Int? = null
)

@Suppress("unused")
@Document("ChallengeDefaultPoints")
open class ChallengeDefaultPoints(
    @Id var id: ObjectId? = null,
    var assignedFor: String?,
    var questionnaires: Int?,
    var news: Int?,
    var activities: Int?,
    var challenges: Int?
)

@Document("ChallengeDefinition")
open class ChallengeDefinition(
    @field:Schema(type = "string")
    @Id var id: ObjectId? = null,
    @field:Schema(type = "string")
    var createdById: ObjectId? = null,
    var preWarn: Boolean? = null,
    var deleted: Boolean? = null
) : CommonChallenge() {
    /* fun toProgramming(): ChallengeProgramming{
        val programming = ChallengeProgramming(
            definitionId = this.id
        )
        val definition = this
        programming.apply {
            title = definition.title
            description = definition.description
            explanation = definition.explanation
            periodDays = definition.periodDays
            periodAmount = definition.periodAmount
            category = definition.category
            subcategory = definition.subcategory
            points = definition.points
            assignedFor = definition.assignedFor
            type = definition.type
            minAge = definition.minAge
            maxAge = definition.maxAge
            lockDown = definition.lockDown
            didacticUnitsIds = definition.didacticUnitsIds
            steps = definition.steps
            childRelation = definition.childRelation
            specificIds = definition.specificIds
            location = definition.location
            questionnaire = definition.questionnaire
            questionnaireCorrectAnswer = definition.questionnaireCorrectAnswer
            news = definition.news
        }
        return programming
    }// */
}

@Suppress("unused")
@Document("ChallengeProgramming")
open class ChallengeProgramming(
    @field:Schema(type = "string")
    @Id var id: ObjectId? = null,
    @field:Schema(type = "string")
    var definitionId: ObjectId? = null,
    @field:Schema(type = "string")
    var programedById: ObjectId? = null,
    var startDatetime: Instant? = null,
    var finishDatetime: Instant? = null,
    var visibleDatetime: Instant? = null,
    @field:ArraySchema(schema = Schema(type = "string"))
    var overridesIds: Array<ObjectId>? = null,
    var assignedToIds: AssignedToIds? = null
) : CommonChallenge() {
    @Transient
    var completions: Int? = null
}

@Document("ChallengeCompletion")
open class ChallengeCompletion(
    @field:Schema(type = "string")
    @Id var id: ObjectId? = null,
    @field:Schema(type = "string")
    var definitionId: ObjectId? = null,
    @field:Schema(type = "string")
    var programmingId: ObjectId? = null,
    @field:Schema(type = "string")
    var forId: ObjectId? = null,
    var displayName: String? = null,
    //TODO ia: Define how we'll receive that from ChallengeProgramming
    @field:ArraySchema(schema = Schema(type = "string"))
    var overridesIds: Array<ObjectId>? = null,
    var periodStartDatetime: Instant? = null,
    var periodFinishDatetime: Instant? = null,
    var visibleDatetime: Instant? = null,
    var completed: Boolean? = null,
    var completionDatetime: Instant? = null,
    var completedByParents: List<CompletedByParent>? = null,
    var deleted: Boolean? = null,
    //Physical
    var userSteps: Int? = null,
    //Authority
    @field:Schema(type = "string")
    var approvedById: ObjectId? = null,
    //GPS
    var userLocation: ChallengeLocationPoint? = null,
    var precision: Double? = null,
    //Questionnaire
    var questionnaireAnswer: String? = null,
    //News
    var newsAnswer: String? = null
) : CommonChallenge() {
    @Transient
    var important: Boolean? = null

    @Transient
    var active: Boolean? = null

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other !is ChallengeCompletion) return false

        return this.programmingId == other.programmingId && this.forId == other.forId
                && this.periodStartDatetime == other.periodStartDatetime
                && this.periodFinishDatetime == other.periodFinishDatetime
    }

    override fun hashCode(): Int {
        return this.id.hashCode()
    }

    fun getAwardedPoints(): Int {
        val completedByChild = this.completed == true
        val completedByParent = this.completedByParents?.isNotEmpty() == true
        return if (completedByChild && completedByParent) {
            this.points?.both ?: 0
        } else if (completedByChild) {
            this.points?.onlyChild ?: 0
        } else if (completedByParent) {
            this.points?.onlyParent ?: 0
        } else {
            0
        }
    }
}

/**
 * Defines ChallengeDefinition common base properties
 */
open class CommonChallenge(
    var title: String? = null,
    var description: String? = null,
    var explanation: String? = null,
    var periodDays: Long? = null,
    var periodAmount: Int? = null,
    var category: String? = null,
    var subcategory: String? = null,
    var points: ChallengePoints? = null,
    var assignedFor: Array<String>? = null,
    var type: String? = null,
    var minAge: Int? = null,
    var maxAge: Int? = null,
    var lockDown: Boolean? = null,
    var didacticUnitsIds: List<ObjectId>? = null,
    //Physical
    var steps: Int? = null,
    //Authority
    var childRelation: Array<String>? = null,
    @field:ArraySchema(schema = Schema(type = "string"))
    var specificIds: Array<ObjectId>? = null,
    //GPS
    var location: ChallengeLocationPolygon? = null,
    //Questionnaire
    var questionnaire: String? = null,
    var questionnaireCorrectAnswer: String? = null,
    //News
    var news: String? = null
)