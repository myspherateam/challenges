package com.mysphera.paido.controller

import com.mysphera.paido.*
import com.mysphera.paido.data.ChallengeCompletionRepository
import com.mysphera.paido.service.CommonChallengeService
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest

@Suppress("unused")
@RestController
@RequestMapping("/admin")
class AdminController : AbstractController() {

    @Autowired
    private lateinit var challengeCompletionRepository: ChallengeCompletionRepository

    @Autowired
    private lateinit var commonChallengeService: CommonChallengeService

    data class RecalculateStatisticsResponse(
        val successes: Int,
        val failures: Int,
        val successIds: List<ObjectId>,
        val failureResponses: List<ResponseEntity<Response<*>>>
    )

    @GetMapping("/recalculateStatistics")
    fun recalculateStatistics(request: HttpServletRequest): Response<RecalculateStatisticsResponse> {
        denyIfNotValidRoles(request, listOf(Rol.GLOBAL_ADMIN))
        val loginMap = tools.doLogin()
        val token = loginMap["access_token"] as String

        val failures = mutableListOf<ResponseEntity<Response<*>>>()
        val processed = mutableListOf<ObjectId>()

        // Get all users
        val pageSize = 100
        var page = 0
        var users = tools.findUsers(
            "Bearer $token",
            roles = listOf(Rol.CHILD.type, Rol.PARENT.type),
            pageRequest = getPageRequest(page, pageSize)
        )
        while (users.isNotEmpty()) {

            loop@ for (user in users) {

                val userId = user.id ?: continue

                commonChallengeService.recalculateData(user)
                println("Adding user $userId with ${user.points} points and ${user.steps} steps")
                val response = tools.http.doPOST(
                    request.getHeader(HttpHeaders.AUTHORIZATION),
                    "${tools.administrationEndpoint}/users",
                    user
                )
                if (response.statusCode.value() != HttpStatus.OK.value()) {
                    println("User $userId Failed")
                    failures.add(response)
                } else {
                    println("User $userId OK")
                    processed.add(userId)
                }
            }

            page += 1
            users = tools.findUsers("Bearer $token", pageRequest = getPageRequest(page, pageSize))
        }
        return Response(
            RecalculateStatisticsResponse(
                processed.size,
                failures.size,
                processed,
                failures
            )
        )
    }

    @GetMapping("/recalculateBadges")
    fun recalculateBadges(request: HttpServletRequest): Response<String> {
        throw ServerException("Not implemented")
    }
}