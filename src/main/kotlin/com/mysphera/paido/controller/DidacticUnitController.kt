package com.mysphera.paido.controller

import com.mysphera.paido.*
import com.mysphera.paido.data.AssignedToIds
import com.mysphera.paido.data.ChallengeProgramming
import com.mysphera.paido.data.DidacticUnit
import com.mysphera.paido.service.DidacticUnitService
import com.mysphera.paido.service.ProgrammingService
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*
import java.time.Instant
import java.time.temporal.ChronoUnit
import javax.servlet.http.HttpServletRequest

@Suppress("unused")
@RestController
@RequestMapping("/didacticUnit")
class DidacticUnitController : AbstractController() {

    @Value("\${challenge-periods-limit}")
    private lateinit var periodsLimit: String

    @Autowired
    private lateinit var didacticUnitService: DidacticUnitService

    @Autowired
    private lateinit var programmingService: ProgrammingService

    @PostMapping
    fun post(request: HttpServletRequest, @RequestBody unit: DidacticUnit): Response<DidacticUnit> {
        val callerUser =
            denyIfNotValidRoles(request, listOf(Rol.CLINICIAN, Rol.TEACHER, Rol.LOCAL_ADMIN))

        unit.createdById = callerUser.resolveUserId()

        return Response(
            if (unit.id == null) {
                didacticUnitService.save(unit)
            } else {
                didacticUnitService.edit(unit)
            }
        )
    }

    @GetMapping
    fun getMany(
        request: HttpServletRequest,
        @RequestParam(required = false) page: Int?,
        @RequestParam(required = false) pageSize: Int?,
        @RequestParam(required = false) ids: List<ObjectId>?,
        @RequestParam(required = false) name: String?
    ): Response<List<DidacticUnit>> {
        denyIfNotValidRoles(request, listOf(Rol.CLINICIAN, Rol.TEACHER, Rol.LOCAL_ADMIN))

        val result =
            didacticUnitRepo.findWithOptionalFilters(getPageRequest(page, pageSize), ids, name)

        return Response(result.data, count = result.total)
    }

    @GetMapping("/{id}")
    fun getOne(
        request: HttpServletRequest,
        @PathVariable("id") id: ObjectId
    ): Response<DidacticUnit> {
        denyIfNotValidRoles(request, listOf(Rol.CLINICIAN, Rol.TEACHER, Rol.LOCAL_ADMIN))

        return Response(didacticUnitRepo.findById(id)
            .orElseThrow { throw NotFoundException("No DidacticUnit was found") })
    }

    @PostMapping("/delete")
    fun deleteOne(
        request: HttpServletRequest,
        @RequestBody unit: DidacticUnit
    ): Response<DidacticUnit> {
        val callerUser =
            denyIfNotValidRoles(request, listOf(Rol.LOCAL_ADMIN, Rol.CLINICIAN, Rol.TEACHER))

        val unitId = unit.id ?: throw BadRequestException("id is required")

        didacticUnitRepo.findById(unitId).orElseThrow {
            throw BadRequestException("id provided was not found")
        }.apply {
            deleted = true
            deletedById = callerUser.resolveUserId()
        }.also {
            return Response(didacticUnitRepo.save(it))
        }
    }

    data class DidacticUnitProgramming(
        var startDatetime: Instant,
        var assignedToIds: AssignedToIds
    )

    @PostMapping("/{id}/program")
    fun program(
        request: HttpServletRequest,
        @PathVariable("id") id: ObjectId,
        @RequestBody unit: DidacticUnitProgramming
    ): Response<List<ChallengeProgramming>> {
        val callerUser =
            denyIfNotValidRoles(request, listOf(Rol.LOCAL_ADMIN, Rol.CLINICIAN, Rol.TEACHER))
        val callerToken = request.getHeader(HttpHeaders.AUTHORIZATION)

        val didacticUnit = didacticUnitRepo.findById(id).orElseThrow {
            throw BadRequestException("id provided was not found")
        }

        val challenges = didacticUnit.challenges
            ?: throw DataException("the programming has no challenges to program")

        val programmings = challenges.map { didacticUnitChallenge ->
            val definition =
                definitionRepo.findById(didacticUnitChallenge.challengeId).orElseThrow {
                    throw DataException("challengeDefinition not found from id ${didacticUnitChallenge.challengeId}")
                }
            val startDatetime =
                unit.startDatetime.plus(didacticUnitChallenge.startOffset, ChronoUnit.DAYS)
            val programming = ChallengeProgramming(
                definitionId = definition.id,
                programedById = callerUser.id,
                visibleDatetime = startDatetime,
                startDatetime = startDatetime,
                finishDatetime = unit.startDatetime.plus(
                    didacticUnitChallenge.endOffset,
                    ChronoUnit.DAYS
                ),
                assignedToIds = unit.assignedToIds
            )

            val forIdUsers = programmingService.validateProgramming(programming, callerToken)
            Pair(programming, forIdUsers)
        }

        val results = programmings.map {
            programmingService.createProgrammingAndCompletions(
                it.first, it.second, periodsLimit.toLong()
            )
        }

        return Response(results)
    }
}