package com.mysphera.paido.controller

import com.mysphera.paido.*
import com.mysphera.paido.data.*
import mu.KotlinLogging
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest

@Suppress("unused")
@RestController
abstract class AbstractController {
    protected val logger = KotlinLogging.logger { }

    @Autowired
    protected lateinit var tools: MicroServicesToolbox

    @Autowired
    protected lateinit var definitionRepo: ChallengeDefinitionRepository

    @Autowired
    protected lateinit var programmingRepo: ChallengeProgrammingRepository

    @Autowired
    protected lateinit var completionRepo: ChallengeCompletionRepository

    @Autowired
    protected lateinit var didacticUnitRepo: DidacticUnitRepository

    @Autowired
    protected lateinit var badgeRepo: BadgeRepository

    fun denyIfNotValidRoles(request: HttpServletRequest, validRoles: List<Rol>): User {
        val authHeader = request.getHeader(HttpHeaders.AUTHORIZATION)
        val callerId = request.getJWTPayload().id
        val childId: String? = request.getParameter("childId")

        return tools.findUser(authHeader, callerId, childId)
            .apply {
                validRoles.find { hasRol(it) }
                    ?: throw ForbiddenException("Allowed user roles: ${validRoles.joinToString(", ")}")
            }
    }

    fun denyAccessIfNotAdministration(userAgent: String?) {
        if (!userAgentIs(userAgent, "administration")) {
            throw ForbiddenException()
        }
    }

    private fun userAgentIs(userAgent: String?, value: String): Boolean {
        return userAgent?.toLowerCase() == value
    }

    fun isOriginalUser(request: HttpServletRequest): Boolean {
        val callerId = request.getJWTPayload().id
        val originalId = request.getJWTPayload().originalId
        return callerId == originalId
    }
    fun denyIfNotValidUser(request: HttpServletRequest, userId: ObjectId): User {
        val callerId = request.getJWTPayload().id
        val authHeader = request.getHeader(HttpHeaders.AUTHORIZATION)
        val childId: String? = request.getParameter("childId")

        return tools.findUser(authHeader, callerId, childId)
            .apply {
                if (callerId !== userId && !this.isParentOf(userId)){
                    throw MissingAuthenticationException("Token does not match with user")
                }
            }
    }
}