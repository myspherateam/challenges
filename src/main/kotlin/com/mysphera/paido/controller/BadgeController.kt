package com.mysphera.paido.controller

import com.mysphera.paido.*
import com.mysphera.paido.data.Badge
import com.mysphera.paido.data.UserBadges
import com.mysphera.paido.service.BadgeService
import net.coobird.thumbnailator.Thumbnails
import org.bson.types.Binary
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.CacheControl
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.io.ByteArrayInputStream
import java.util.concurrent.TimeUnit
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Suppress("unused")
@RestController
@RequestMapping("/badge")
class BadgeController : AbstractController() {
    @Autowired
    private lateinit var badgeService: BadgeService

    @GetMapping
    fun getMany(
        request: HttpServletRequest,
        @RequestParam(required = false) page: Int?,
        @RequestParam(required = false) pageSize: Int?,
        @RequestParam(required = false) ids: List<ObjectId>?,
        @RequestParam(required = false) name: String?,
        @RequestParam(required = false) includePictures: Boolean?
    ): Response<List<Badge>> {
        denyIfNotValidRoles(
            request,
            listOf(Rol.CLINICIAN, Rol.TEACHER, Rol.LOCAL_ADMIN, Rol.PARENT, Rol.CHILD)
        )

        val result = badgeRepo.findWithOptionalFilters(
            getPageRequest(page, pageSize),
            ids,
            name,
            includePictures
        )

        return Response(result.data, count = result.total)
    }

    @PostMapping
    fun create(
        request: HttpServletRequest,
        @RequestBody(required = false) body: ByteArray?,
        @RequestParam(required = false) id: ObjectId?,
        @RequestParam(required = false) name: String?,
        @RequestParam(required = false) description: String?
    ): Response<Badge> {
        val callerUser =
            denyIfNotValidRoles(request, listOf(Rol.CLINICIAN, Rol.TEACHER, Rol.LOCAL_ADMIN))
        val badge = Badge(
            id,
            callerUser.resolveUserId(),
            name,
            description,
            body?.let { Binary(it) }
        )

        val savedBadge = Response(
            if (badge.id == null) {
                badgeService.save(badge)
            } else {
                badgeService.edit(badge)
            }
        )
        savedBadge.message.image = null

        return savedBadge
    }

    @PostMapping("/base64")
    fun create(request: HttpServletRequest, @RequestBody badge: Badge): Response<Badge> {
        val callerUser =
            denyIfNotValidRoles(request, listOf(Rol.CLINICIAN, Rol.TEACHER, Rol.LOCAL_ADMIN))

        badge.createdById = callerUser.resolveUserId()

        val savedBadge = Response(
            if (badge.id == null) {
                badgeService.save(badge)
            } else {
                badgeService.edit(badge)
            }
        )

        savedBadge.message.image = null

        return savedBadge
    }

    @GetMapping("/{id}")
    fun getOne(request: HttpServletRequest, @PathVariable("id") id: ObjectId): Response<Badge> {
        denyIfNotValidRoles(
            request,
            listOf(Rol.CLINICIAN, Rol.TEACHER, Rol.LOCAL_ADMIN, Rol.PARENT, Rol.CHILD)
        )

        return Response(badgeRepo.findById(id)
            .orElseThrow { throw NotFoundException("No Badge was found") })
    }

    @GetMapping("/{id}/image")
    fun getImage(
        response: HttpServletResponse,
        request: HttpServletRequest,
        @PathVariable("id") id: ObjectId
    ): ResponseEntity<*> {
        denyIfNotValidRoles(
            request,
            listOf(Rol.CLINICIAN, Rol.TEACHER, Rol.LOCAL_ADMIN, Rol.PARENT, Rol.CHILD)
        )

        val badge = Response(badgeRepo.findById(id)
            .orElseThrow { throw NotFoundException("No Badge was found") })

        val image = badge.message.image ?: throw NotFoundException("No image in Badge")

        response.outputStream.write(
            image.data
        )

        val headerValue = CacheControl.maxAge(365, TimeUnit.DAYS).headerValue

        response.addHeader("Cache-Control", headerValue)
        response.addHeader("Content", "img/jpeg")

        return ResponseEntity<Any?>(HttpStatus.OK)
    }

    @GetMapping("/{id}/thumb")
    fun getThumbnail(
        response: HttpServletResponse,
        request: HttpServletRequest,
        @PathVariable("id") id: ObjectId,
        @RequestParam(required = false) height: Int?,
        @RequestParam(required = false) width: Int?
    ): ResponseEntity<*> {
        denyIfNotValidRoles(
            request,
            listOf(Rol.CLINICIAN, Rol.TEACHER, Rol.LOCAL_ADMIN, Rol.PARENT, Rol.CHILD)
        )

        val badge = Response(badgeRepo.findById(id)
            .orElseThrow { throw NotFoundException("No Badge was found") })

        val image = badge.message.image ?: throw NotFoundException("No image in Badge")

        val thumbnail = Thumbnails.of(
            ByteArrayInputStream(
                image.data
            )
        )

        when {
            height != null -> {
                thumbnail.height(height)
            }
            width != null -> {
                thumbnail.width(width)
            }
            else -> {
                thumbnail.scale(1.0)
            }
        }

        thumbnail.toOutputStream(response.outputStream)

        val headerValue = CacheControl.maxAge(365, TimeUnit.DAYS).headerValue

        response.addHeader("Cache-Control", headerValue)
        response.addHeader("Content", "img/jpeg")

        return ResponseEntity<Any?>(HttpStatus.OK)
    }

    @GetMapping("/forId/{userId}")
    fun getBadgesForId(
        request: HttpServletRequest,
        @PathVariable("userId") forId: ObjectId,
        @RequestParam(required = false) page: Int?,
        @RequestParam(required = false) pageSize: Int?,
        @RequestParam(required = false) includePictures: Boolean?
    ): Response<UserBadges> {
        val caller = denyIfNotValidRoles(
            request,
            listOf(Rol.TEACHER, Rol.CLINICIAN, Rol.LOCAL_ADMIN, Rol.PARENT, Rol.CHILD)
        )

        val user = if (caller.id == forId) {
            caller
        } else {
            val authHeader = request.getHeader(HttpHeaders.AUTHORIZATION)
            tools.findUser(authHeader, forId, null)
        }

        val result = badgeRepo.getBadgesFromUser(user, includePictures)
        if(result.badges.isEmpty()&&result.futureBadges.isEmpty()){
          result.futureBadges = listOf(this.badgeService.pointsBadgeStatus(user)!!)
        }

        return Response(result)
    }

    @GetMapping("/nextBadge")
    fun getNextBadge(
        request: HttpServletRequest,
        @RequestParam(required = true) type: String
    ): Response<Badge?> {
        val caller = denyIfNotValidRoles(request, listOf(Rol.CHILD))

        val result = when (type) {
            "points" -> badgeService.pointsBadgeStatus(caller)
            else -> throw BadRequestException("Type $type not recognized (valid types: points)")
        }

        return Response(result)
    }
}