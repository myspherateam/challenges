package com.mysphera.paido.controller

import com.mysphera.paido.*
import com.mysphera.paido.data.AssignedToIds
import com.mysphera.paido.data.ChallengeCompletion
import com.mysphera.paido.data.ChallengeProgramming
import com.mysphera.paido.data.SwaggerExamples
import com.mysphera.paido.service.ProgrammingService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.Parameters
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.ExampleObject
import io.swagger.v3.oas.annotations.media.Schema
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest

@Suppress("unused")
@RestController
@RequestMapping("/challengeProgramming")
class ChallengeProgrammingController : AbstractController() {
    @Value("\${challenge-periods-limit}")
    private lateinit var periodsLimit: String

    @Autowired
    private lateinit var programmingService: ProgrammingService


    @Parameters(
        Parameter(
            `in` = ParameterIn.QUERY,
            name = "page",
            required = false,
            schema = Schema(type = "integer")
        ),
        Parameter(
            `in` = ParameterIn.QUERY,
            name = "pageSize",
            required = false,
            schema = Schema(type = "integer")
        ),
        Parameter(
            `in` = ParameterIn.QUERY,
            name = "entities",
            required = false,
            schema = Schema(type = "string")
        ),
        Parameter(
            `in` = ParameterIn.QUERY,
            name = "groups",
            required = false,
            schema = Schema(type = "string")
        ),
        Parameter(
            `in` = ParameterIn.QUERY,
            name = "users",
            required = false,
            schema = Schema(type = "string")
        )
    )
    @GetMapping
    fun getMany(
        request: HttpServletRequest,
        @RequestParam(required = false) page: Int?,
        @RequestParam(required = false) pageSize: Int?,
        @RequestParam(required = false) entities: List<ObjectId>?,
        @RequestParam(required = false) groups: List<ObjectId>?,
        @RequestParam(required = false) users: List<ObjectId>?
    ): Response<List<ChallengeProgramming>> {
        val callerUser = denyIfNotValidRoles(request, listOf(Rol.TEACHER, Rol.CLINICIAN))

        val callerEntities = callerUser.entityIds ?: emptyList()
        val filteredEntities = when {
            entities.isNullOrEmpty() -> callerEntities
            else -> {
                val providedNotAllowed = entities.filter { !callerEntities.contains(it) }

                if (providedNotAllowed.isNotEmpty()) {
                    throw ForbiddenException("Some entities provided are not allowed for that user")
                } else {
                    entities
                }
            }
        }

        val result = programmingRepo.findWithOptionalFilters(
            getPageRequest(page, pageSize), filteredEntities, groups, users
        )

        return Response(result.data, count = result.total)
    }

    @GetMapping("/{challengeId}/users")
    fun getProgrammingUsers(
        request: HttpServletRequest,
        @PathVariable("challengeId") id: ObjectId
    ): Response<List<ChallengeCompletion>> {
        denyIfNotValidRoles(request, listOf(Rol.TEACHER, Rol.CLINICIAN))

        val completions = completionRepo.findByProgrammingIdAndForIdAndCompleted(programmingId = id)
            .reversed().distinctBy { it.forId }

        return Response(completions, count = completions.size)
    }

    @Parameters(
        Parameter(
            `in` = ParameterIn.PATH,
            name = "challengeId",
            allowEmptyValue = false,
            schema = Schema(type = "string")
        )
    )
    @GetMapping("/{challengeId}")
    fun getOne(
        request: HttpServletRequest,
        @PathVariable("challengeId") id: ObjectId
    ): Response<ChallengeProgramming> {
        denyIfNotValidRoles(request, listOf(Rol.TEACHER, Rol.CLINICIAN))

        return Response(programmingRepo.findById(id)
            .orElseThrow { throw NotFoundException("No ChallengeProgramming was found") })
    }

    @PostMapping("/{challengeId}/cancel")
    fun cancel(
        request: HttpServletRequest,
        @PathVariable("challengeId") id: ObjectId,
        @RequestBody assignedToIds: AssignedToIds
    ): Response<List<ChallengeCompletion>> {
        denyIfNotValidRoles(request, listOf(Rol.LOCAL_ADMIN, Rol.TEACHER, Rol.CLINICIAN))

        return Response(
            programmingService.cancel(
                id,
                assignedToIds,
                request.getHeader(HttpHeaders.AUTHORIZATION)
            )
        )
    }

    @PostMapping("/refresh")
    fun refresh(request: HttpServletRequest, @RequestBody newUser: User): Response<Int> {
        denyAccessIfNotAdministration(request.getHeader(HttpHeaders.USER_AGENT))

        if (!newUser.hasRol(Rol.CHILD)) {
            throw ForbiddenException("No child user")
        }

        return Response(programmingService.refresh(newUser, periodsLimit.toLong()))
    }

    @Operation(
        requestBody = io.swagger.v3.oas.annotations.parameters.RequestBody(
            content = [Content(
                schema = Schema(implementation = ChallengeProgramming::class),
                examples = [
                    ExampleObject(
                        SwaggerExamples.NEW_CHALLENGE_PROGRAMMING,
                        name = SwaggerExamples.NEW_CHALLENGE_PROGRAMMING_NAME,
                        description = SwaggerExamples.NEW_CHALLENGE_PROGRAMMING_DESC
                    ),
                    ExampleObject(
                        SwaggerExamples.NEW_CHALLENGE_PROGRAMMING_OVERRIDING,
                        name = SwaggerExamples.NEW_CHALLENGE_PROGRAMMING_OVERRIDING_NAME,
                        description = SwaggerExamples.NEW_CHALLENGE_PROGRAMMING_OVERRIDING_DESC
                    )
                ]
            )]
        ), responses = [io.swagger.v3.oas.annotations.responses.ApiResponse(
            content = [Content(
                schema = Schema(implementation = Response::class),
                examples = [
                    ExampleObject(
                        SwaggerExamples.RESPONSE_EXAMPLE,
                        name = SwaggerExamples.RESPONSE_NAME,
                        description = SwaggerExamples.RESPONSE_DESC
                    )
                ]
            )]
        )]
    )
    @PostMapping
    fun createOne(
        request: HttpServletRequest,
        @RequestBody programming: ChallengeProgramming
    ): Response<ChallengeProgramming> {
        val callerUser = denyIfNotValidRoles(request, listOf(Rol.TEACHER, Rol.CLINICIAN))
        val callerToken = request.getHeader(HttpHeaders.AUTHORIZATION)

        programming.programedById = callerUser.resolveUserId()
        val forIdUsers = programmingService.validateProgramming(programming, callerToken)

        return Response(
            programmingService.createProgrammingAndCompletions(
                programming, forIdUsers, periodsLimit.toLong()
            )
        )
    }
}