package com.mysphera.paido.controller

import com.mysphera.paido.*
import com.mysphera.paido.data.*
import com.mysphera.paido.service.BadgeService
import com.mysphera.paido.service.CompletionService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.Parameters
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.ExampleObject
import io.swagger.v3.oas.annotations.media.Schema
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Sort
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.time.Instant
import javax.servlet.http.HttpServletRequest

@Suppress("unused")
@RestController
@RequestMapping("/challengeCompletion")
class ChallengeCompletionController : AbstractController() {
    @Autowired
    private lateinit var completionService: CompletionService

    @Autowired
    private lateinit var badgeService: BadgeService

    @Operation(
        requestBody = io.swagger.v3.oas.annotations.parameters.RequestBody(
            content = [Content(
                schema = Schema(implementation = ChallengeCompletion::class),
                examples = [
                    ExampleObject(
                        SwaggerExamples.NEW_CHALLENGE_COMPLETION,
                        name = SwaggerExamples.NEW_CHALLENGE_COMPLETION_NAME,
                        description = SwaggerExamples.NEW_CHALLENGE_COMPLETION_DESC
                    )
                ]
            )]
        ), responses = [io.swagger.v3.oas.annotations.responses.ApiResponse(
            content = [Content(
                schema = Schema(implementation = Response::class),
                examples = [
                    ExampleObject(
                        SwaggerExamples.RESPONSE_EXAMPLE,
                        name = SwaggerExamples.RESPONSE_NAME,
                        description = SwaggerExamples.RESPONSE_DESC
                    )
                ]
            )]
        )]
    )
    @PostMapping
    fun updateOne(
        request: HttpServletRequest,
        @RequestBody completion: ChallengeCompletion
    ): Response<ChallengeCompletion> {
        val callerUser =
            denyIfNotValidRoles(request, listOf(Rol.PARENT, Rol.CHILD, Rol.TEACHER, Rol.CLINICIAN))

        val caller = callerUser.resolveUserId()
        val callerIsParent = callerUser.hasRol(Rol.PARENT)
        val callerIsParentAsChild = callerUser.hasRol(Rol.PARENT) && !isOriginalUser(request)
        val callerIsChild = callerUser.hasRol(Rol.CHILD)
        val callerIsTeacherOrClinician =
            callerUser.hasRol(Rol.TEACHER) || callerUser.hasRol(Rol.CLINICIAN)

        with(completion) {
            val completionId = id ?: throw BadRequestException("id is required")
            val storedCompletion = completionRepo.findById(completionId)
                .orElseThrow { throw BadRequestException("The id provided doesn't exists") }

            val storedForId = storedCompletion.forId
                ?: throw DataException("No forId on ChallengeCompletion ($completionId)")

            val assignedForList = storedCompletion.assignedFor
                ?: throw DataException("No assignedFor on ChallengeCompletion ($completionId)")
            val assignedForChild = assignedForList.contains(AssignedFor.CHILD.type)
            val assignedForParent = assignedForList.contains(AssignedFor.PARENT.type)

            //Check caller has permission to complete
            when {
                (callerIsChild && caller != storedForId) || (callerIsChild && !assignedForChild) -> {
                    throw ForbiddenException("Child is not assigned")
                }
                callerIsParent && callerUser.isParentOf(storedForId) && !assignedForParent && storedCompletion.type != ChallengeType.AUTHORITY.type -> {
                    throw ForbiddenException("Challenge is not assigned to Parent's Children")
                }
            }

            //Check challenge is already completed by caller
            val isAlreadyCompletedByCaller = when {
                forId != null && storedCompletion.type == ChallengeType.AUTHORITY.type -> completionService.isCompletedByChild(
                    storedCompletion
                )
                callerIsChild -> completionService.isCompletedByChild(storedCompletion)
                callerIsParent -> completionService.isCompletedByParent(storedCompletion, caller)
                callerIsTeacherOrClinician -> completionService.isCompleted(storedCompletion)
                else -> return Response(storedCompletion)
            }

            if (isAlreadyCompletedByCaller) {
                return Response(storedCompletion)
            }

            val now = Instant.now()

            //Challenge cant be completed if not started
            if (now < storedCompletion.periodStartDatetime) {
                throw ForbiddenException("The challenge cant be completed. It doesn't have started yet")
            }

            //Challenge cant be completed if expired
            if (now > storedCompletion.periodFinishDatetime) {
                throw ForbiddenException("The challenge cant be completed. Its expired")
            }

            //Checking the challenge was not overriden for the user
            if (!storedCompletion.overridesIds.isNullOrEmpty()) {
                throw ForbiddenException("The challenge was override")
            }

            //Checking if teacher/clinician is trying to complete other than authority type challenges
            if (callerIsTeacherOrClinician && storedCompletion.type != ChallengeType.AUTHORITY.type) {
                throw ForbiddenException("Teacher/Clinician can complete Authority type challenges only")
            }

            val forIdUser = tools.findUser(
                request.getHeader(HttpHeaders.AUTHORIZATION),
                storedForId,
                childId = request.getParameter("childId")
            )

            val specificTypeFieldsAreValid: Boolean = when (storedCompletion.type) {
                ChallengeType.PHYSICAL.type -> {
                    userSteps?.let {
                        if (callerIsParentAsChild) {
                            throw ForbiddenException("Parent user as Child cant complete Physical type challenges")
                        }

                        storedCompletion.userSteps = it
                        if (it < 0) {
                            throw BadRequestException("userSteps must be greater than 0")
                        } else {
                            it >= (storedCompletion.steps
                                ?: throw DataException("No steps on ChallengeCompletion ($completionId)"))
                        }
                    } ?: throw BadRequestException("userSteps is required")
                }
                ChallengeType.GPS.type -> {
                    val polygonCoordinates =
                        storedCompletion.location?.coordinates?.first() ?: emptyArray()

                    precision?.let {
                        storedCompletion.precision = it

                        if (it < 0) {
                            throw BadRequestException("precision must be greater than 0")
                        }

                        userLocation?.let { point ->
                            storedCompletion.userLocation = point

                            point.coordinates?.let { coordinates ->
                                if (coordinates.size != 2) {
                                    throw BadRequestException("coordinates must contain pairs of doubles")
                                }

                                isPointInsidePolygon(coordinates, polygonCoordinates)
                            } ?: throw BadRequestException("coordinates are required")
                        } ?: throw BadRequestException("userLocation is required")
                    } ?: throw BadRequestException("precision is required")
                }
                ChallengeType.QUESTIONNAIRE.type -> {
                    val questionnaire = storedCompletion.questionnaire
                        ?: throw DataException("Questionnaire type challenge with no questionnaire")
                    val userAnswer = questionnaireAnswer
                        ?: throw BadRequestException("questionnaireAnswer is required")
                    storedCompletion.questionnaireAnswer = userAnswer
                    storedCompletion.questionnaireCorrectAnswer?.let { correctAnswer ->
                        completionService.validateQuestionnaire(
                            completionId,
                            questionnaire,
                            correctAnswer,
                            userAnswer
                        )
                    } ?: true
                }
                ChallengeType.NEWS.type -> {
                    if (!storedCompletion.questionnaire.isNullOrEmpty()) {
                        val questionnaire = storedCompletion.questionnaire
                            ?: throw DataException("Questionnaire type challenge with no questionnaire")
                        val userAnswer =
                            newsAnswer ?: throw BadRequestException("newsAnswer is required")
                        storedCompletion.newsAnswer = userAnswer
                        storedCompletion.questionnaireCorrectAnswer?.let { correctAnswer ->
                            completionService.validateQuestionnaire(
                                completionId, questionnaire, correctAnswer, userAnswer
                            )
                        }
                    }

                    true
                }
                ChallengeType.AUTHORITY.type -> {
                    if (callerIsChild) {
                        throw ForbiddenException("User ($caller) cant complete Authority type challenges. It's a Child?")
                    }

                    if (storedCompletion.specificIds.isNullOrEmpty()) {
                        val storedChildRelation = storedCompletion.childRelation
                            ?: throw DataException("No childRelation on ChallengeCompletion ($completionId)")

                        when {
                            storedChildRelation.contains(ChildRelation.TEACHER.type) || storedChildRelation.contains(
                                ChildRelation.CLINICIAN.type
                            ) -> {
                                if (callerIsTeacherOrClinician) {
                                    val childEntities = forIdUser.entityIds ?: emptyList()
                                    val callerUserEntities = callerUser.entityIds ?: emptyList()
                                    val isTeacherOrClinicianOfChild = callerUserEntities.any {
                                        childEntities.contains(it)
                                    }
                                    if (!isTeacherOrClinicianOfChild) {
                                        throw ForbiddenException("User ($caller) cant complete the challenge. It isn't related with user")
                                    }
                                } else {
                                    throw ForbiddenException("User ($caller) cant complete the challenge. It isn't related with user")
                                }
                            }
                            storedChildRelation.contains(ChildRelation.PARENT.type) -> {
                                val callerIsParentOfChild =
                                    callerIsParent && callerUser.isParentOf(storedForId)
                                if (!callerIsParentOfChild) {
                                    throw ForbiddenException("User ($caller) cant complete the challenge. It isn't related with user")
                                }
                            }
                        }
                    } else {
                        val callerIsSpecificId =
                            storedCompletion.specificIds?.contains(caller) ?: false
                        if (!callerIsSpecificId) {
                            throw ForbiddenException("User ($caller) cant complete the challenge. It isn't a 'specificIds' user")
                        }
                    }

                    storedCompletion.approvedById = caller
                    true
                }
                else -> {
                    throw DataException("No valid type was found ($completionId)")
                }
            }

            if (specificTypeFieldsAreValid) {
                when {
                    callerIsTeacherOrClinician || (callerIsChild && assignedForChild) || (storedCompletion.type == ChallengeType.AUTHORITY.type && forId != null) -> {
                        //Complete from Child or Parent in name of child
                        //Complete from Teacher/Clinician (Challenge Authority with assignedFor Child always)
                        storedCompletion.completed = true
                        storedCompletion.completionDatetime = now
                        forIdUser.steps = (forIdUser.steps ?: 0) + (userSteps ?: 0)
                        forIdUser.numCompletedChallenges =
                            (forIdUser.numCompletedChallenges ?: NumChallenges.empty).plus(
                                storedCompletion
                            )
                    }
                    callerIsParent && assignedForParent -> {
                        //Complete from Parent
                        storedCompletion.completedByParents =
                            storedCompletion.completedByParents?.plus(
                                CompletedByParent(
                                    caller,
                                    now
                                )
                            )
                        callerUser.steps = (callerUser.steps ?: 0) + (userSteps ?: 0)
                        callerUser.numCompletedChallenges =
                            (callerUser.numCompletedChallenges ?: NumChallenges.empty).plus(
                                storedCompletion
                            )
                        tools.http.doPOST(
                            request.getHeader(HttpHeaders.AUTHORIZATION),
                            "${tools.administrationEndpoint}/users",
                            callerUser
                        )
                    }
                }

                //storedCompletion = completionRepo.save(storedCompletion)

                if (completionService.canGetPoints(storedCompletion)) {
                    val points = storedCompletion.points
                        ?: throw DataException("Completion has no points ($completionId)")
                    val pointAmount = when {
                        callerIsTeacherOrClinician -> {
                            //If teacher/clinician is trying to complete a challenge, then the challenge type is authority
                            //Authority type challenge is assigned for child only, so get child points only.
                            points.onlyChild
                                ?: throw DataException("Challenge assignedFor Child without points.onlyChild (CompletionId: $completionId)")
                        }
                        assignedForChild && assignedForParent -> {
                            val childPoints = points.onlyChild
                                ?: throw DataException("Challenge assignedFor Child and Parent without points.onlyChild (CompletionId: $completionId)")
                            val parentPoints = points.onlyParent
                                ?: throw DataException("Challenge assignedFor Parent and Child without points.onlyParent (CompletionId: $completionId)")
                            val bothPoints = points.both
                                ?: throw DataException("Challenge assignedFor Child and Parent without points.both (CompletionId: $completionId)")
                            val whateverPoints =
                                childPoints == parentPoints && parentPoints == bothPoints

                            when {
                                whateverPoints && callerIsParent -> {
                                    storedCompletion.completed = true
                                    storedCompletion.completionDatetime = now
                                    bothPoints
                                }
                                whateverPoints && callerIsChild -> {
                                    forIdUser.parents?.forEach {
                                        storedCompletion.completedByParents =
                                            storedCompletion.completedByParents?.plus(
                                                CompletedByParent(it, now)
                                            )
                                    }
                                        ?: throw DataException("Child has no parents (${forIdUser.id})")

                                    bothPoints
                                }
                                !whateverPoints && callerIsParent -> {
                                    if (storedCompletion.completedByParents?.any { it.parentId != callerUser.id } == true) {
                                        0
                                    } else {
                                        if (storedCompletion.completed == true) {
                                            bothPoints - childPoints
                                        } else {
                                            parentPoints
                                        }
                                    }
                                }
                                !whateverPoints && callerIsChild -> {
                                    if (storedCompletion.completedByParents?.isNotEmpty() == true) {
                                        bothPoints - parentPoints
                                    } else {
                                        childPoints
                                    }
                                }
                                else -> throw DataException("Challenge points has no valid values ($completionId)")
                            }
                        }
                        assignedForChild -> {
                            points.onlyChild
                                ?: throw DataException("Challenge assignedFor Child without points.onlyChild ($completionId)")
                        }
                        assignedForParent -> {
                            points.onlyParent
                                ?: throw DataException("Challenge assignedFor Parent without points.onlyParent ($completionId)")
                        }
                        else -> throw DataException("Challenge assignedFor has no valid values ($completionId)")
                    }

                    forIdUser.points = forIdUser.points?.plus(pointAmount)

                }
            }

            val stored = completionRepo.save(storedCompletion)

            badgeService.awardBadges(forIdUser, stored)
            badgeService.nextBadges(forIdUser)


            val response = tools.http.doPOST(
                request.getHeader(HttpHeaders.AUTHORIZATION),
                "${tools.administrationEndpoint}/users",
                forIdUser
            )
            if (response.statusCode.value() != HttpStatus.OK.value()) {
                throw ServerException(response)
            }

            return Response(stored)
        }
    }

    @Parameters(
        Parameter(
            `in` = ParameterIn.QUERY,
            name = "finished",
            required = false,
            schema = Schema(type = "boolean")
        )
    )
    @GetMapping("/count")
    fun count(
        request: HttpServletRequest,
        @RequestParam(required = false) started: Boolean?,
        @RequestParam(required = false) finished: Boolean?
    ): Response<ChallengeCount> {
        val callerUser = denyIfNotValidRoles(request, listOf(Rol.CHILD, Rol.PARENT))
        val caller = callerUser.resolveUserId()

        val forIds = when {
            callerUser.hasRol(Rol.PARENT) -> {
                callerUser.caringForIds
                    ?: throw DataException("Parent user without 'caringForIds' ($caller)")
            }
            else -> {
                // If user is not Parent role, it should be child or it would have been denied before reach that point
                listOf(caller)
            }
        }

        return Response(
            completionRepo.findGroupedByCategoriesAndSubcategories(
                forIds, caller.takeIf { callerUser.hasRol(Rol.PARENT) }, started, finished
            )
        )
    }

    @GetMapping("/forId/{userId}")
    fun getByForId(
        request: HttpServletRequest, @PathVariable("userId") forId: ObjectId
    ): Response<List<ChallengeCompletion>> {
        denyIfNotValidRoles(request, listOf(Rol.TEACHER, Rol.CLINICIAN))

        val completions = completionRepo.findByProgrammingIdAndForIdAndCompleted(forId = forId)

        return Response(completions, count = completions.size)
    }

    @Parameters(
        Parameter(
            `in` = ParameterIn.QUERY,
            name = "page",
            required = false,
            schema = Schema(type = "integer")
        ),
        Parameter(
            `in` = ParameterIn.QUERY,
            name = "pageSize",
            required = false,
            schema = Schema(type = "integer")
        ),
        Parameter(
            `in` = ParameterIn.QUERY,
            name = "finished",
            required = false,
            schema = Schema(type = "boolean")
        ),
        Parameter(
            `in` = ParameterIn.QUERY,
            name = "type",
            required = false,
            schema = Schema(type = "string")
        ),
        Parameter(
            `in` = ParameterIn.QUERY,
            name = "category",
            required = false,
            schema = Schema(type = "string")
        ),
        Parameter(
            `in` = ParameterIn.QUERY,
            name = "subcategory",
            required = false,
            schema = Schema(type = "string")
        )
    )
    @GetMapping
    fun getMany(
        request: HttpServletRequest,
        @RequestParam(required = false) page: Int?,
        @RequestParam(required = false) pageSize: Int?,
        @RequestParam(required = false) started: Boolean?,
        @RequestParam(required = false) finished: Boolean?,
        @RequestParam(required = false) completed: Boolean?,
        @RequestParam(required = false) inverted: Boolean?,
        @RequestParam(required = false) type: String?,
        @RequestParam(required = false) category: String?,
        @RequestParam(required = false) subcategory: String?
    ): Response<List<ChallengeCompletion>> {
        val callerUser = denyIfNotValidRoles(request, listOf(Rol.CHILD, Rol.PARENT))
        val caller = callerUser.resolveUserId()

        val forIds = when {
            callerUser.hasRol(Rol.PARENT) -> {
                callerUser.caringForIds
                    ?: throw DataException("Parent user without 'caringForIds' ($caller)")
            }
            else -> {
                // If user is not Parent role, it should be child or it would have been denied before reach that point
                listOf(caller)
            }
        }

        val result = completionRepo.findWithOptionalFilters(
            childList = forIds, parent = caller.takeIf { callerUser.hasRol(Rol.PARENT) },
            category = category, subcategory = subcategory, type = type,
            started = started, finished = finished ?: false, completed = completed ?: false,
            inverted = inverted, pageable = getPageRequest(page, pageSize)
        )

        val sortedCompletions = result.data
            .onEach {
                it.important = completionService.isCompletionImportantFor(it, callerUser)
                it.active = completionService.isCompletionActive(it)
            }
            .sortedByDescending { it.important }

        return Response(sortedCompletions, count = result.total)
    }

    @Parameters(
        Parameter(
            `in` = ParameterIn.PATH,
            name = "challengeId",
            allowEmptyValue = false,
            schema = Schema(type = "string")
        )
    )
    @GetMapping("/{challengeId}")
    fun getOne(
        request: HttpServletRequest,
        @PathVariable("challengeId") id: ObjectId
    ): Response<ChallengeCompletion> {
        val callerUser = denyIfNotValidRoles(request, listOf(Rol.PARENT, Rol.CHILD))

        val callerIsParent = callerUser.hasRol(Rol.PARENT)
        val callerIsChild = callerUser.hasRol(Rol.CHILD)

        val completion = completionRepo.findById(id).orElseThrow {
            throw NotFoundException("No ChallengeCompletion was found")
        }
        val completionForId = completion.forId ?: throw DataException("Completion has no forId")

        if (callerIsChild && callerUser.resolveUserId() != completionForId) {
            throw ForbiddenException("Non related challenge for that Child (Child)")
        }
        if (callerIsParent && !callerUser.isParentOf(completionForId)) {
            throw ForbiddenException("Non related challenge for that Child (Parent)")
        }

        completion.important = completionService.isCompletionImportantFor(completion, callerUser)
        completion.active = completionService.isCompletionActive(completion)

        return Response(completion)
    }

    @GetMapping("/nextPhysical")
    fun getNextPhysical(request: HttpServletRequest): Response<List<ChallengeCompletion>> {
        val callerUser = denyIfNotValidRoles(request, listOf(Rol.PARENT, Rol.CHILD))
        val caller = callerUser.resolveUserId()

        val forIds = when {
            callerUser.hasRol(Rol.PARENT) -> {
                callerUser.caringForIds
                    ?: throw DataException("Parent user without 'caringForIds' ($caller)")
            }
            else -> {
                // If user is not Parent role, it should be child or it would have been denied before reach that point
                listOf(caller)
            }
        }

        val result = completionRepo.findWithOptionalFilters(
            childList = forIds,
            type = ChallengeType.PHYSICAL.type,
            parent = caller.takeIf { callerUser.hasRol(Rol.PARENT) },
            started = true,
            finished = false,
            sortOrder = Sort.Direction.ASC,
            sortFields = listOf("periodFinishDatetime"),
            completed = false
        ).data.filter { completionService.isCompletionImportantFor(it, callerUser) }
            .take(forIds.size).onEach { it.important = true }

        return Response(result, count = result.size)
    }
}