package com.mysphera.paido.controller

import com.mysphera.paido.Response
import com.mysphera.paido.data.NotificationDevice
import com.mysphera.paido.service.NotificationService

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/notifications")
class NotificationController : AbstractController() {
    @Autowired
    private lateinit var notificationService: NotificationService

    @PostMapping("/device")
    fun createOne(
        request: HttpServletRequest,
        @RequestBody device: NotificationDevice
    ): Response<NotificationDevice> {
        val user = denyIfNotValidUser(request,device.userId)
        notificationService.addDevice(device)
        return Response(device)
    }

}