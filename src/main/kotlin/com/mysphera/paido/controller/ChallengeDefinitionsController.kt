package com.mysphera.paido.controller

import com.mysphera.paido.*
import com.mysphera.paido.data.ChallengeDefaultPoints
import com.mysphera.paido.data.ChallengeDefinition
import com.mysphera.paido.data.SwaggerExamples
import com.mysphera.paido.service.CommonChallengeService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.Parameters
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.ExampleObject
import io.swagger.v3.oas.annotations.media.Schema
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest

@Suppress("unused")
@RestController
@RequestMapping("/challengeDefinitions")
class ChallengeDefinitionsController : AbstractController() {
    @Autowired
    private lateinit var commonChallengeService: CommonChallengeService

    @GetMapping("/defaultPoints")
    fun defaultPoints(@RequestParam(required = false) assignedFor: String?): Response<List<ChallengeDefaultPoints>> {
        return Response(definitionRepo.findDefaultPoints(assignedFor))
    }

    @Operation(
        requestBody = io.swagger.v3.oas.annotations.parameters.RequestBody(
            content = [Content(
                schema = Schema(implementation = ChallengeDefinition::class),
                examples = [
                    ExampleObject(
                        SwaggerExamples.NEW_CHALLENGE_DEFINITION,
                        name = SwaggerExamples.NEW_CHALLENGE_DEFINITION_NAME,
                        description = SwaggerExamples.NEW_CHALLENGE_DEFINITION_DESC
                    )
                ]
            )]
        ), responses = [io.swagger.v3.oas.annotations.responses.ApiResponse(
            content = [Content(
                schema = Schema(implementation = Response::class),
                examples = [
                    ExampleObject(
                        SwaggerExamples.RESPONSE_EXAMPLE,
                        name = SwaggerExamples.RESPONSE_NAME,
                        description = SwaggerExamples.RESPONSE_DESC
                    )
                ]
            )]
        )]
    )
    @PostMapping
    fun createOne(
        request: HttpServletRequest, @RequestBody definition: ChallengeDefinition
    ): Response<ChallengeDefinition> {
        val callerUser = denyIfNotValidRoles(request, listOf(Rol.TEACHER, Rol.CLINICIAN))

        commonChallengeService.validateCommonFields(
            request.getHeader(HttpHeaders.AUTHORIZATION),
            definition
        )

        val definitionId = definition.id

        val toStore = if (definitionId == null) {
            definition.apply { createdById = callerUser.resolveUserId() }
        } else {
            definitionRepo.findById(definitionId)
                .orElseThrow { throw BadRequestException("id provided was not found") }
                .apply {
                    if (type != definition.type) {
                        throw BadRequestException("type cant be modified")
                    }

                    preWarn = definition.preWarn
                    //createdById & deleted wont be modified
                    title = definition.title
                    description = definition.description
                    explanation = definition.explanation
                    periodDays = definition.periodDays
                    periodAmount = definition.periodAmount
                    category = definition.category
                    subcategory = definition.subcategory
                    points = definition.points
                    assignedFor = definition.assignedFor
                    minAge = definition.minAge
                    maxAge = definition.maxAge
                    lockDown = definition.lockDown
                    didacticUnitsIds = definition.didacticUnitsIds
                    steps = definition.steps
                    childRelation = definition.childRelation
                    specificIds = definition.specificIds
                    location = definition.location
                    questionnaire = definition.questionnaire
                    questionnaireCorrectAnswer = definition.questionnaireCorrectAnswer
                    news = definition.news
                }
        }

        return Response(definitionRepo.save(toStore))
    }

    @PostMapping("/delete")
    fun deleteOne(
        request: HttpServletRequest, @RequestBody definition: ChallengeDefinition
    ): Response<ChallengeDefinition> {
        denyIfNotValidRoles(request, listOf(Rol.TEACHER, Rol.CLINICIAN, Rol.LOCAL_ADMIN))

        val definitionId = definition.id ?: throw BadRequestException("id is required")

        definitionRepo.findById(definitionId)
            .orElseThrow { throw BadRequestException("id provided was not found") }
            .apply { deleted = true }.also { return Response(definitionRepo.save(it)) }
    }

    @Parameters(
        Parameter(
            `in` = ParameterIn.QUERY,
            name = "page",
            required = false,
            schema = Schema(type = "integer")
        ),
        Parameter(
            `in` = ParameterIn.QUERY,
            name = "pageSize",
            required = false,
            schema = Schema(type = "integer")
        ),
        Parameter(
            `in` = ParameterIn.QUERY,
            name = "type",
            required = false,
            schema = Schema(type = "string")
        ),
        Parameter(
            `in` = ParameterIn.QUERY,
            name = "category",
            required = false,
            schema = Schema(type = "string")
        ),
        Parameter(
            `in` = ParameterIn.QUERY,
            name = "subcategory",
            required = false,
            schema = Schema(type = "string")
        )
    )
    @GetMapping
    fun getMany(
        request: HttpServletRequest,
        @RequestParam(required = false) page: Int?,
        @RequestParam(required = false) pageSize: Int?,
        @RequestParam(required = false) type: String?,
        @RequestParam(required = false) category: String?,
        @RequestParam(required = false) subcategory: String?
    ): Response<List<ChallengeDefinition>> {
        denyIfNotValidRoles(request, listOf(Rol.TEACHER, Rol.CLINICIAN))

        val result = definitionRepo.findWithOptionalFilters(
            getPageRequest(page, pageSize),
            category,
            subcategory,
            type
        )

        return Response(result.data, count = result.total)
    }

    @Parameters(
        Parameter(
            `in` = ParameterIn.PATH,
            name = "challengeId",
            allowEmptyValue = false,
            schema = Schema(type = "string")
        )
    )
    @GetMapping("/{challengeId}")
    fun getOne(
        request: HttpServletRequest,
        @PathVariable("challengeId") id: ObjectId
    ): Response<ChallengeDefinition> {
        denyIfNotValidRoles(request, listOf(Rol.TEACHER, Rol.CLINICIAN))

        return Response(definitionRepo.findById(id)
            .orElseThrow { throw NotFoundException("No ChallengeDefinition was found") })
    }
}


