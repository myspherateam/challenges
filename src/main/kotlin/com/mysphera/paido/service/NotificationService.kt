package com.mysphera.paido.service

import com.mysphera.paido.data.NotificationDevice
import com.mysphera.paido.data.NotificationRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class NotificationService {
    @Autowired
    lateinit var repository: NotificationRepository

    fun addDevice(device:NotificationDevice): NotificationDevice {
        val value = repository.findByToken(device.token)
        if (value === null){
            return repository.save(device)
        }
        return value
    }
}