package com.mysphera.paido.service

import com.mysphera.paido.*
import com.mysphera.paido.data.*
import mu.KotlinLogging
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.Instant
import java.time.temporal.ChronoUnit

@Service
class ProgrammingService {
    private val logger = KotlinLogging.logger {}

    @Autowired
    private lateinit var tools: MicroServicesToolbox

    @Autowired
    private lateinit var completionRepo: ChallengeCompletionRepository

    @Autowired
    private lateinit var programmingRepo: ChallengeProgrammingRepository

    @Autowired
    private lateinit var definitionRepo: ChallengeDefinitionRepository

    @Autowired
    private lateinit var commonChallengeService: CommonChallengeService

    @Autowired
    private lateinit var firebaseService: FirebaseService

    @Autowired
    private lateinit var notificationService: NotificationService

    fun cancel(
        programmingId: ObjectId,
        assignedToIds: AssignedToIds,
        token: String
    ): List<ChallengeCompletion> {
        val usersIds = assignedToIds.users ?: emptyList()
        val groupsIds = assignedToIds.groups ?: emptyList()
        val entitiesIds = assignedToIds.entities ?: emptyList()

        val resolvedUsersIds = when {
            usersIds.isNotEmpty() -> usersIds
            groupsIds.isNotEmpty() -> {
                tools.findUsers(token, groups = groupsIds).map { it.resolveUserId() }
            }
            entitiesIds.isNotEmpty() -> {
                tools.findUsers(token, entities = entitiesIds).map { it.resolveUserId() }
            }
            else -> {
                throw BadRequestException("assignedToIds provided cant be empty.")
            }
        }

        programmingRepo.findById(programmingId).orElseThrow {
            throw BadRequestException("programmingId provided was not found")
        }.also {
            it.assignedToIds?.excludedUsersIds =
                (it.assignedToIds?.excludedUsersIds ?: listOf()).toMutableSet()
                    .plus(resolvedUsersIds.toSet()).toList()
            programmingRepo.save(it)
        }

        val matched = completionRepo.findWithOptionalFilters(
            childList = resolvedUsersIds, programmingId = programmingId,
            finished = false, visible = false
        ).data.map {
            it.deleted = true
            it
        }

        completionRepo.saveAll(matched)

        return matched
    }

    fun refresh(newUser: User, periodLimit: Long): Int {
        val forId = newUser.resolveUserId()
        val entityIds =
            newUser.entityIds ?: throw DataException("The user has no entities ($newUser.id)")
        val groupIds = newUser.groupIds

        val alreadyExistingProgramming = programmingRepo.findWithOptionalFilters(
            entities = entityIds,
            groups = groupIds,
            expired = false
        )

        val completionsToStore = mutableListOf<ChallengeCompletion>()
        alreadyExistingProgramming.data.forEach { programming ->
            val programmingId = programming.id ?: throw DataException("Challenge has no id")
            if (!completionRepo.existsByForIdAndProgrammingId(forId, programmingId)) {
                val today = Instant.now()
                val programmingFinishDatetime = programming.finishDatetime
                    ?: throw DataException("Challenge has no finishDatetime ($programmingId)")
                val programmingPeriodDays = programming.periodDays ?: 0

                val completions = when {
                    programmingPeriodDays > 0 && today < programmingFinishDatetime -> {
                        var limitedCompletions: List<ChallengeCompletion>

                        var i = 1
                        do {
                            limitedCompletions = getCompletionsByProgrammingAndForIds(
                                programming,
                                listOf(newUser),
                                periodLimit * i
                            )

                            val lastPeriodChallenge = limitedCompletions.maxBy {
                                it.periodFinishDatetime
                                    ?: throw DataException("Completion has no periodFinishDatetime (${it.id})")
                            }
                                ?: throw DataException("No next completions for programming: $programmingId")
                            val isExpired =
                                lastPeriodChallenge.periodFinishDatetime!! < Instant.now()

                            i++
                        } while (isExpired)

                        limitedCompletions
                    }
                    else -> {
                        getCompletionsByProgrammingAndForIds(programming, listOf(newUser))
                    }
                }

                completionsToStore.addAll(completions)
            }
        }

        return completionRepo.saveAll(completionsToStore).size
    }

    fun getCompletionsByProgrammingAndForIds(
        p: ChallengeProgramming,
        forIdUsers: List<User>,
        limit: Long? = null
    ): List<ChallengeCompletion> {
        val periods = mutableListOf<Pair<Instant, Instant>>()

        val periodDaysValue = p.periodDays ?: 0
        if (periodDaysValue > 0) {
            val truncatedStartDatetime = p.startDatetime?.truncatedTo(ChronoUnit.DAYS)
                ?: throw BadRequestException("startDatetime must not be null")
            val truncatedFinishDatetime = p.finishDatetime?.truncatedTo(ChronoUnit.DAYS)
                ?: throw BadRequestException("finishDatetime must not be null")
            val days = (ChronoUnit.DAYS.between(
                truncatedStartDatetime,
                truncatedFinishDatetime
            ) + 1).toInt()

            val initPeriodStartDatetime = p.startDatetime
                ?: throw DataException("ChallengeProgramming must have startDatetime ($p.id)")
            val initPeriodFinishDatetime =
                initPeriodStartDatetime.plusSeconds(periodDaysValue * 86400 - 1)

            val times = days / periodDaysValue
            val max = limit ?: (times + 1)
            var i = 0
            while (i < times && periods.size < max) {
                val newPeriodStartDatetime =
                    initPeriodStartDatetime.plus(periodDaysValue * i, ChronoUnit.DAYS)
                val newPeriodFinisDatetime =
                    initPeriodFinishDatetime.plus(periodDaysValue * i, ChronoUnit.DAYS)
                periods.add(Pair(newPeriodStartDatetime, newPeriodFinisDatetime))
                i++
            }
            if (days % periodDaysValue != 0L && periods.size < max) {
                //Add remaining non-periodic days (ex. 5 days duration with 2 period -> remains 1 non-periodic day)
                val lastPeriodFinishDatetime =
                    if (periods.isEmpty()) truncatedStartDatetime else periods.last().second
                var newPeriodStartDatetime = lastPeriodFinishDatetime.plus(1, ChronoUnit.DAYS)
                newPeriodStartDatetime = newPeriodStartDatetime.truncatedTo(ChronoUnit.DAYS)
                val newPeriodFinishDatetime = p.finishDatetime
                    ?: throw DataException("ChallengeProgramming must have finishDatetime ($p.id)")
                periods.add(Pair(newPeriodStartDatetime, newPeriodFinishDatetime))
            }
        } else {
            val newPeriodStartDatetime = p.startDatetime
                ?: throw DataException("ChallengeProgramming must have startDatetime ($p.id)")
            val newPeriodFinishDatetime = p.finishDatetime
                ?: throw DataException("ChallengeProgramming must have finishDatetime ($p.id)")
            periods.add(Pair(newPeriodStartDatetime, newPeriodFinishDatetime))
        }

        val completions = mutableListOf<ChallengeCompletion>()

        forIdUsers.forEach { forIdUser ->
            periods.forEach { range ->
                val newCompletion = ChallengeCompletion().apply {
                    definitionId = p.definitionId
                    programmingId = p.id
                    forId = forIdUser.id ?: throw DataException("user.id")
                    displayName = forIdUser.resolveDescription()
                    completed = false
                    completedByParents = emptyList()

                    //visibleDatetime = if(range.first == p.startDatetime) p.visibleDatetime else range.first
                    visibleDatetime = p.visibleDatetime
                    periodStartDatetime = range.first
                    periodFinishDatetime = range.second

                    //Copy from programming
                    title = p.title
                    description = p.description
                    explanation = p.explanation
                    periodDays = p.periodDays
                    periodAmount = p.periodAmount
                    category = p.category
                    subcategory = p.subcategory
                    points = p.points
                    assignedFor = p.assignedFor
                    type = p.type
                    minAge = p.minAge
                    maxAge = p.maxAge
                    lockDown = p.lockDown
                    didacticUnitsIds = p.didacticUnitsIds
                    steps = p.steps //Physical
                    childRelation = p.childRelation //Authority
                    specificIds = p.specificIds //Authority
                    location = p.location //GPS
                    questionnaire = p.questionnaire //Questionnaire
                    questionnaireCorrectAnswer = p.questionnaireCorrectAnswer //Questionnaire
                    news = p.news //News
                }

                completions.add(newCompletion)
            }
        }

        return completions
    }

    fun getCompletionsByProgramming(
        p: ChallengeProgramming,
        token: String
    ): List<ChallengeCompletion> {
        val entities = p.assignedToIds?.entities
        val groups = p.assignedToIds?.groups
        val users = p.assignedToIds?.users

        val forIdUsers =
            tools.findUsers(token, users, groups, entities).filter { it.hasRol(Rol.CHILD) }

        return getCompletionsByProgrammingAndForIds(p, forIdUsers)
    }

    fun createNextCompletions(limit: Int): Int {
        var resultCount = 0

        val loginMap = tools.doLogin()
        val token = loginMap["access_token"] as String

        programmingRepo.findPeriodicAndNotExpired().forEach { programming ->
            val programmingId =
                programming.id ?: throw DataException("ChallengeProgramming has no id")
            val excludedForIds = programming.assignedToIds?.excludedUsersIds ?: emptyList()

            val allCompletions = getCompletionsByProgramming(programming, "Bearer $token")
                .filterNot { excludedForIds.contains(it.forId) }
            val storedCompletions =
                completionRepo.findByProgrammingIdAndForIdAndCompleted(programmingId = programmingId)
                    .filterNot { excludedForIds.contains(it.forId) }

            if (storedCompletions.size < allCompletions.size) {
                val lastPeriod = storedCompletions.maxBy {
                    it.periodFinishDatetime
                        ?: throw DataException("ChallengeCompletion has no periodFinishDatetime (${it.id})")
                }
                    ?: throw DataException("ChallengeProgramming ($programmingId) with no ChallengeCompletion")
                val startPeriod = lastPeriod.periodStartDatetime
                    ?: throw DataException("ChallengeCompletion has no periodStartDatetime (${lastPeriod.id})")
                val endPeriod = lastPeriod.periodFinishDatetime
                    ?: throw DataException("ChallengeCompletion has no periodFinishDatetime (${lastPeriod.id})")

                val today = Instant.now()
                if (today in startPeriod..endPeriod || today > endPeriod) {
                    val notStored = allCompletions.filterNot {
                        storedCompletions.contains(it)
                    }

                    //If server was down, we need to take in mind challenges could have started since server was down
                    val alreadyStarted = notStored.filter {
                        val periodStart = it.periodStartDatetime
                            ?: throw DataException("Completion has no periodStartDatetime (${it.id})")
                        periodStart < today
                    }
                    if (alreadyStarted.isNotEmpty()) {
                        logger.info { "Already started challenges: ${alreadyStarted.size}" }
                    }

                    //Pending challenges to be started soon
                    val nonStarted = notStored.subtract(alreadyStarted.toSet())
                        .groupBy { it.forId ?: throw DataException("Completion has no forId") }
                        .flatMap { it.value.take(limit) }

                    //TODO add sum to numAssignedChallenges
                    val toStore = alreadyStarted.plus(nonStarted)

                    if (toStore.isNotEmpty()) {
                        val stored = completionRepo.saveAll(toStore)
                        resultCount += stored.size
                    }
                }
            }
        }
        return resultCount
    }

    fun createProgrammingAndCompletions(
        programming: ChallengeProgramming,
        forIdUsers: List<User>,
        periodLimit: Long?
    ): ChallengeProgramming {
        val savedProgramming = programmingRepo.save(programming)
        val notificationDevices = mutableListOf<String>()
        val completionsToStore = getCompletionsByProgrammingAndForIds(
            savedProgramming,
            forIdUsers,
            periodLimit
        )

        val savedCompletions = completionRepo.saveAll(completionsToStore)

        val loginMap = tools.doLogin()
        val token = loginMap["access_token"] as String
        forIdUsers.forEach {
            commonChallengeService.recalculateData(it)
            tools.http.doPOST(
                "Bearer $token",
                "${tools.administrationEndpoint}/users",
                it
            )
            if (it.id!==null){
                val deviceTokens = notificationService.repository.findByIdUser(it.id!!).map {device-> device.token }
                notificationDevices.addAll(deviceTokens)
            }

        }
        if (notificationDevices.size>0){
            firebaseService.sendNotification("¡Nuevo Reto! ${programming.title}", programming.description , notificationDevices)
        }

        //Setting transient fields before return
        savedProgramming.completions = savedCompletions.size

        return savedProgramming
    }

    fun validateProgramming(programming: ChallengeProgramming, token: String): List<User> {
        val forIdUsers = mutableListOf<User>()

        with(programming) {
            val definition = definitionId?.let {
                definitionRepo.findById(it).orElseThrow {
                    throw BadRequestException("The definitionId provided doesn't exists")
                }
            } ?: throw BadRequestException("definitionId is required")

            val startDatetime =
                startDatetime ?: throw BadRequestException("startDatetime is required")
            val finishDatetime =
                finishDatetime ?: throw BadRequestException("finishDatetime is required")
            if (startDatetime >= finishDatetime) {
                throw BadRequestException("startDatetime cant be greater/equals than finishDatetime")
            }

            periodDays?.let { period ->
                val truncatedStartDatetime = startDatetime.truncatedTo(ChronoUnit.DAYS)
                val truncatedFinishDatetime = finishDatetime.truncatedTo(ChronoUnit.DAYS)
                val days = 1 + ChronoUnit.DAYS.between(
                    truncatedStartDatetime,
                    truncatedFinishDatetime
                )

                if (period > days) {
                    throw BadRequestException("periodDays can't be greater than days between startDatetime and finishDatetime (both inclusive)")
                }

                val maxAmount = days / period
                periodAmount?.let { amount ->
                    if (amount > maxAmount) {
                        throw BadRequestException("periodAmount can't be greater than highest amount of period we would create [ (finishDatetime - startDatetime) / periodDays ]")
                    }
                }
            }

            val assignedTo = assignedToIds ?: throw BadRequestException("assignedToIds is required")
            forIdUsers.addAll(validateAndGetAssignedToIdsChildList(assignedTo, token))

            val preWarn = definition.preWarn ?: false
            visibleDatetime = if (preWarn) {
                visibleDatetime
                    ?: throw BadRequestException("visibleDatetime is required (definition.preWarn is true)")
            } else {
                startDatetime
            }

            title = title ?: definition.title
            description = description ?: definition.description
            explanation = explanation ?: definition.explanation
            periodDays = periodDays ?: definition.periodDays
            periodAmount = periodAmount ?: definition.periodAmount
            category = category ?: definition.category
            subcategory = subcategory ?: definition.subcategory
            points = points ?: definition.points
            assignedFor = assignedFor ?: definition.assignedFor
            type = definition.type //Cant be overriden
            minAge = minAge ?: definition.minAge
            maxAge = maxAge ?: definition.maxAge
            lockDown = lockDown ?: definition.lockDown
            didacticUnitsIds = didacticUnitsIds ?: definition.didacticUnitsIds
            steps = steps ?: definition.steps //Physical
            childRelation = childRelation ?: definition.childRelation //Authority
            specificIds = specificIds ?: definition.specificIds //Authority
            location = location ?: definition.location //GPS
            questionnaire = questionnaire ?: definition.questionnaire //Questionnaire
            questionnaireCorrectAnswer =
                questionnaireCorrectAnswer ?: definition.questionnaireCorrectAnswer
            news = news ?: definition.news //News

            commonChallengeService.validateCommonFields(token, programming)
        }

        return forIdUsers
    }

    fun validateAndGetAssignedToIdsChildList(
        assignedToIds: AssignedToIds,
        token: String
    ): List<User> {
        with(assignedToIds) {
            val excludedUsersIds = excludedUsersIds ?: emptyList()

            entities = entities?.distinct()
            groups = groups?.distinct()
            users = users?.distinct()

            return when {
                !users.isNullOrEmpty() && !groups.isNullOrEmpty() && !entities.isNullOrEmpty() -> {
                    tools.findUsers(token, users, groups, entities)
                }
                !groups.isNullOrEmpty() && !entities.isNullOrEmpty() -> {
                    tools.findUsers(token, groups = groups, entities = entities)
                }
                !entities.isNullOrEmpty() -> {
                    tools.findUsers(token, entities = entities)
                }
                else -> {
                    throw BadRequestException("assignedToIds is bad requested")
                }
            }.filter { it.hasRol(Rol.CHILD) }
                .filterNot { excludedUsersIds.contains(it.id) }
                .ifEmpty { throw BadRequestException("Provided assignedToIds were not found") }
        }
    }
}