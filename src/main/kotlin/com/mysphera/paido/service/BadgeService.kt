package com.mysphera.paido.service

import com.mysphera.paido.BadRequestException
import com.mysphera.paido.DataException
import com.mysphera.paido.User
import com.mysphera.paido.data.*
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class BadgeService {
    @Autowired
    private lateinit var repository: BadgeRepository

    @Autowired
    private lateinit var didacticUnitRepository: DidacticUnitRepository

    @Autowired
    private lateinit var challengeCompletionRepository: ChallengeCompletionRepository

    private fun validate(badge: Badge) {
        badge.name?.let {
            if (it.trim().isEmpty()) {
                throw BadRequestException("name was not valid")
            }
        } ?: throw BadRequestException("name was not provided")
    }

    fun save(badge: Badge): Badge {
        validate(badge)

        return repository.save(badge)
    }

    fun edit(badge: Badge): Badge {
        val badgeId = badge.id ?: throw BadRequestException("id is required to edit")

        val storedBadge = repository.findById(badgeId).orElseThrow {
            throw BadRequestException("The provided id doesn't exists")
        }

        storedBadge.name = badge.name ?: storedBadge.name
        storedBadge.description = badge.description ?: storedBadge.description
        storedBadge.image = badge.image ?: storedBadge.image

        validate(storedBadge)

        return repository.save(storedBadge)
    }

    fun awardBadges(user: User, storedCompletion: ChallengeCompletion?) {

        val userId = user.id ?: throw DataException("User did not have a valid id")

        val badges = repository.getBadgesFromUser(user, false)

        val badgesToAward: MutableList<ObjectId> = mutableListOf()
        //Check for DidacticUnitBadge
        val didacticUnitIds = if (storedCompletion != null) {
            storedCompletion.didacticUnitsIds ?: listOf()
        } else {
            val challenge =
                challengeCompletionRepository.findWithOptionalFilters(childList = listOf(userId)).data
            challenge.mapNotNull { it.didacticUnitsIds }.flatten().toSet().toList()
        }

        val units = didacticUnitRepository.findAllById(didacticUnitIds)
        val didacticUnitBadges = units.mapNotNull { unit ->
            val challengeIds = unit.challenges?.map { it.challengeId } ?: listOf()
            val challenges = challengeCompletionRepository.findAllById(challengeIds)
            val allCompleted = challenges.all { challenge -> challenge.completed == true }
            if (allCompleted) {
                unit.badgeId
            } else {
                null
            }
        }.filterNot { badges.badges.any { oldBadge -> oldBadge.id == it } }
        badgesToAward.addAll(didacticUnitBadges)

        //Check for point badges

        val badgesPoints = BadgeConfiguration.PointsBadge.awardedBadges(user)
            .mapNotNull {
                repository.findByCode(it.code).id
            }

        badgesToAward.addAll(badgesPoints)

        user.badges = ((user.badges?.toSet()) ?: setOf()).plus(badgesToAward).toList()
    }

    fun nextBadges(user: User) {

        val userId = user.id ?: throw DataException("User did not have a valid id")

        val challenge =
            challengeCompletionRepository.findWithOptionalFilters(childList = listOf(userId)).data

        val nextBadges: MutableList<ObjectId> = mutableListOf()
        //Check for DidacticUnitBadge
        val didacticUnitIds = challenge.mapNotNull { it.didacticUnitsIds }.flatten().toSet().toList()
        val units = didacticUnitRepository.findAllById(didacticUnitIds)
        val didacticUnitBadges = units.mapNotNull { unit ->
            val challengeIds = unit.challenges?.map { it.challengeId } ?: listOf()
            val challenges = challengeCompletionRepository.findAllById(challengeIds)
            val allCompleted = challenges.all { challenge -> challenge.completed == true }
            if (!allCompleted) {
                unit.badgeId
            } else {
                null
            }
        }
        nextBadges.addAll(didacticUnitBadges)

        //Check for point badges

        val badgesPoints = BadgeConfiguration.PointsBadge.nextRequirement(user)
        val badgeId = badgesPoints?.code?.let { repository.findByCode(it).id }

        if (badgeId != null) {
            nextBadges.add(badgeId)
        }

        user.futureBadges = nextBadges
    }

    fun pointsBadgeStatus(caller: User): Badge? {
        val points = caller.points
        val nextPointsBadgeInfo =
            BadgeConfiguration.PointsBadge.nextRequirement(caller) ?: return null
        val nextPointsBadge = repository.findByCode(nextPointsBadgeInfo.code)
        nextPointsBadge.currentProgress = points
        nextPointsBadge.nextMilestone = nextPointsBadgeInfo.requirement

        return nextPointsBadge
    }
}