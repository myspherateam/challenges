package com.mysphera.paido.service

import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingException
import com.google.firebase.messaging.MulticastMessage
import com.google.firebase.messaging.Notification
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class FirebaseService() {

    @Autowired
    private lateinit var firebaseMessaging: FirebaseMessaging

    fun sendNotification(title:String, message:String?, devices: List<String>){
        val notification = Notification
            .builder()
            .setTitle(title)
            .setBody(message)
            .build()
        val pushMessage = MulticastMessage
            .builder()
            .addAllTokens(devices)
            .setNotification(notification)
            .build()

        try {
           firebaseMessaging.sendMulticast(pushMessage)
        }catch (e:FirebaseMessagingException){
            println(e)
        }
    }
}