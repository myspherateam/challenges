package com.mysphera.paido.service

import com.mysphera.paido.*
import com.mysphera.paido.data.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import kotlin.math.max

@Service
class CommonChallengeService {
    @Autowired
    private lateinit var tools: MicroServicesToolbox

    @Autowired
    private lateinit var didacticUnitRepository: DidacticUnitRepository

    @Autowired
    private lateinit var challengeCompletionRepository: ChallengeCompletionRepository

    @Autowired
    private lateinit var badgeService: BadgeService

    fun validateCommonFields(token: String, c: CommonChallenge) {
        with(c) {
            periodDays?.let {
                if (it <= 0) {
                    throw BadRequestException("periodDays must be greater than 0")
                }
            }
            periodAmount?.let {
                if (it <= 0) {
                    throw BadRequestException("periodAmount must be greater than 0")
                }
            }
            if (title.isNullOrEmpty()) {
                throw BadRequestException("title is required")
            }
            if (description.isNullOrEmpty()) {
                throw BadRequestException("description is required")
            }

            //Type
            type = type?.toLowerCase()?.trim() ?: throw BadRequestException("type is required")

            //Category
            category = category?.let {
                val categoryFiltered = it.toLowerCase().trim()
                val categoryValid = ChallengeCategory.values().any { cat ->
                    cat.type == categoryFiltered
                }
                if (!categoryValid) {
                    throw BadRequestException("Invalid category. (prepare, activate, avoid, enjoy, keep)")
                }

                categoryFiltered
            } ?: throw BadRequestException("category is required")

            //Subcategory
            subcategory = subcategory?.let {
                val subcategoryFiltered = it.toLowerCase().trim()
                val subcategoryValid = ChallengeSubcategory.values().any { subcat ->
                    subcat.type == subcategoryFiltered
                }
                if (!subcategoryValid) {
                    throw BadRequestException("Invalid subcategory. (news, questionnaires, activities, challenges)")
                }

                subcategoryFiltered
            } ?: throw BadRequestException("subcategory is required")

            //AssignedFor
            val assignedForList = assignedFor?.filter {
                AssignedFor.values().map { it2 -> it2.type }.contains(it.toLowerCase().trim())
            }

            assignedFor = if (assignedForList.isNullOrEmpty()) {
                throw BadRequestException("Invalid assignedFor. (child, parent)")
            } else {
                assignedForList.map { it.toLowerCase().trim() }.toTypedArray()
            }

            //Points
            points?.let {
                val assignedForChild = assignedFor?.contains("child") ?: false
                val assignedForParent = assignedFor?.contains("parent") ?: false

                when {
                    assignedForChild && assignedForParent -> {
                        val onlyChild = it.onlyChild
                            ?: throw BadRequestException("points.onlyChild is required. (assignedFor = CHILD, parent)")
                        val onlyParent = it.onlyParent
                            ?: throw BadRequestException("points.onlyParent is required. (assignedFor = child, PARENT)")
                        val both = it.both
                            ?: throw BadRequestException("points.both is required. (assignedFor = child, parent)")
                        it.onlyChild = max(0, onlyChild)
                        it.onlyParent = max(0, onlyParent)
                        it.both = max(0, both)
                    }
                    assignedForChild -> {
                        val onlyChild = it.onlyChild
                            ?: throw BadRequestException("points.onlyChild is required. (assignedFor = child)")
                        it.onlyChild = max(0, onlyChild)
                        it.onlyParent = null
                        it.both = null
                    }
                    assignedForParent -> {
                        val onlyParent = it.onlyParent
                            ?: throw BadRequestException("points.onlyParent is required. (assignedFor = parent)")
                        it.onlyParent = max(0, onlyParent)
                        it.onlyChild = null
                        it.both = null
                    }
                }
            } ?: throw BadRequestException("points are required")

            //Age's
            minAge?.let {
                if (it < 1 || it > 101) {
                    throw BadRequestException("Invalid minAge value. (1-100)")
                }
            }
            maxAge?.let {
                if (it < 1 || it > 100) {
                    throw BadRequestException("Invalid maxAge value. (1-100)")
                }
                if ((minAge ?: 0) > it) {
                    throw BadRequestException("Invalid age range. minAge can't be greater than maxAge.")
                }
            }

            didacticUnitsIds = didacticUnitsIds?.let {
                val ids = it.distinct()
                val result = didacticUnitRepository.findAllById(ids).toList()
                if (result.isEmpty()) {
                    throw BadRequestException("Valid 'didacticUnitsIds' were not found.")
                }
                result.map { unit -> unit.id ?: throw DataException("The DidacticUnit has no id") }
            }

            validateTypeRelatedData(token, c)
            emptyNonRelatedData(c)
        }
    }

    /**
     * Avoid set (and save) useless data according to challenge type
     */
    fun emptyNonRelatedData(c: CommonChallenge) {
        with(c) {
            steps = steps.takeIf { type == ChallengeType.PHYSICAL.type }
            childRelation = childRelation.takeIf { ChallengeType.AUTHORITY.type == type }
            specificIds = specificIds.takeIf { ChallengeType.AUTHORITY.type == type }
            location = location.takeIf { ChallengeType.GPS.type == type }
            news = news.takeIf { ChallengeType.NEWS.type == type }
            questionnaire = questionnaire.takeIf {
                ChallengeType.QUESTIONNAIRE.type == type || ChallengeType.NEWS.type == type
            }
            questionnaireCorrectAnswer = questionnaireCorrectAnswer.takeIf {
                ChallengeType.QUESTIONNAIRE.type == type || ChallengeType.NEWS.type == type
            }
        }
    }

    private fun validateTypeRelatedData(token: String, c: CommonChallenge) {
        when (c.type) {
            ChallengeType.PHYSICAL.type -> validatePhysicalType(c)
            ChallengeType.AUTHORITY.type -> validateAuthorityType(token, c)
            ChallengeType.GPS.type -> validateGPSType(c)
            ChallengeType.QUESTIONNAIRE.type -> validateQuestionnaireType(c)
            ChallengeType.NEWS.type -> validateNewsType(c)
            else -> throw BadRequestException("Invalid type. (physical, authority, gps, questionnaire, news)")
        }
    }

    private fun validatePhysicalType(c: CommonChallenge) {
        val stepsInvalid = c.steps?.let { it <= 0 } ?: true
        if (stepsInvalid) {
            throw BadRequestException("steps must be greater than 0")
        }
    }

    private fun validateAuthorityType(token: String, c: CommonChallenge) {
        with(c) {
            if (childRelation.isNullOrEmpty() && specificIds.isNullOrEmpty()) {
                throw BadRequestException("You must specify a childRelation (parent, teacher, clinician, or many of them) or 'specificIds' values")
            }

            specificIds = specificIds?.let { ids ->
                //Check specificIds existence and not child users (child cant authorize itself)
                val filteredIds = ids.distinct()
                val users = tools.findUsers(token, filteredIds)
                val filteredUsers = users.filter {
                    it.hasRol(Rol.PARENT) || it.hasRol(Rol.TEACHER) || it.hasRol(Rol.CLINICIAN)
                }
                if (filteredUsers.isEmpty()) {
                    throw BadRequestException("Valid 'specificId' users were not found. Take in mind Child user is not valid 'specificId'")
                }
                filteredUsers.map { it.resolveUserId() }.toTypedArray()
            }?.distinct()?.toTypedArray()

            childRelation = if (specificIds.isNullOrEmpty()) {
                childRelation?.map {
                    val filteredRelation = it.toLowerCase().trim()
                    if (!ChildRelation.values().map { it2 -> it2.type }
                            .contains(filteredRelation)) {
                        throw BadRequestException("Invalid childRelation. (parent, teacher, clinician, or many of them)")
                    }
                    filteredRelation
                }?.distinct()?.toTypedArray()
            } else {
                null
            }
        }
    }

    private fun validateGPSType(c: CommonChallenge) {
        with(c) {
            location?.let {
                if (it.type != "Polygon") {
                    throw BadRequestException("'location.type' must be 'Polygon'")
                } else {
                    if (it.coordinates !is Array<Array<Array<Double>>>) {
                        throw BadRequestException("coordinates are not valid")
                    } else {
                        it.coordinates?.let { parent ->
                            parent.forEach { child ->
                                child.forEach { pairs ->
                                    if (pairs.size != 2) {
                                        throw BadRequestException("coordinates must contain pairs of doubles")
                                    }
                                }
                            }
                        }
                    }
                }
            } ?: throw BadRequestException("location is required")
        }
    }

    private fun validateNewsType(c: CommonChallenge) {
        if (c.news.isNullOrEmpty()) {
            throw BadRequestException("news is required")
        }
    }

    private fun validateQuestionnaireType(c: CommonChallenge) {
        if (c.questionnaire.isNullOrEmpty()) {
            throw BadRequestException("questionnaire is required")
        }
    }

    fun recalculateData(user: User) {
        val userId = user.id ?: return

        when {
            user.hasRol(Rol.CHILD) -> {
                val challenges =
                    challengeCompletionRepository.findWithOptionalFilters(childList = listOf(userId)).data
                user.potentialPoints = challenges
                    .map {
                        Collections.max(
                            listOf(
                                it.points?.both ?: 0,
                                it.points?.onlyChild ?: 0,
                                it.points?.onlyParent ?: 0
                            )
                        )
                    }
                    .sum()
                user.points = challenges.map { it.getAwardedPoints() }.sum()
                user.steps = challenges.map {
                    if (it.assignedFor?.contains(Rol.CHILD.type) == true) it.userSteps ?: 0 else 0
                }.sum()
                user.numAssignedChallenges =
                    NumChallenges.fromChallenges(challenges, countChallenge =
                    { it.assignedFor?.contains(Rol.CHILD.type.toLowerCase()) == true }
                    )
                user.numCompletedChallenges = NumChallenges.fromChallenges(
                    challenges,
                    countChallenge = { it.completed == true })
                badgeService.awardBadges(user, null)
                badgeService.nextBadges(user)
            }

            user.hasRol(Rol.PARENT) -> {
                val challenges =
                    challengeCompletionRepository.findWithOptionalFilters(childList = user.caringForIds).data
                user.potentialPoints = challenges
                    .map {
                        Collections.max(
                            listOf(
                                it.points?.both ?: 0,
                                it.points?.onlyChild ?: 0,
                                it.points?.onlyParent ?: 0
                            )
                        )
                    }
                    .sum()
                user.points = challenges.map {
                    if (it.completedByParents?.isNotEmpty() == true) it.points?.onlyParent
                        ?: 0 else 0
                }.sum()
                user.steps = challenges.map {
                    if (it.assignedFor?.contains(Rol.PARENT.type) == true) it.userSteps ?: 0 else 0
                }.sum()
                user.numAssignedChallenges = NumChallenges.fromChallenges(
                    challenges,
                    countChallenge = { it.assignedFor?.contains(Rol.PARENT.type.toLowerCase()) == true })
                user.numCompletedChallenges = NumChallenges.fromChallenges(
                    challenges,
                    countChallenge = { it.completedByParents?.any { cBy -> cBy.parentId == userId } == true })
            }
            else -> return
        }
    }
}