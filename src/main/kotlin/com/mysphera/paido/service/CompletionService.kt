package com.mysphera.paido.service

import com.mysphera.paido.*
import com.mysphera.paido.data.AssignedFor
import com.mysphera.paido.data.ChallengeCompletion
import com.mysphera.paido.data.ChallengeCompletionRepository
import com.mysphera.paido.data.CompletedByParent
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class CompletionService {
    @Autowired
    private lateinit var tools: MicroServicesToolbox

    @Autowired
    private lateinit var completionRepo: ChallengeCompletionRepository

    fun isCompletionActive(c: ChallengeCompletion): Boolean {
        val startDatetime =
            c.periodStartDatetime ?: throw DataException("Completion has no periodStartDatetime")
        val finishDatetime =
            c.periodFinishDatetime ?: throw DataException("Completion has no periodFinishDatetime")

        return Instant.now() in startDatetime..finishDatetime && c.important == true && !isCompleted(
            c
        )
    }

    fun isCompletionImportantFor(c: ChallengeCompletion, callerUser: User): Boolean {
        return when {
            callerUser.hasRol(Rol.PARENT) -> {
                //Important for parent
                val isAssignedToParent = c.assignedFor?.contains(AssignedFor.PARENT.type) ?: false
                val parentHasCompleted =
                    c.completedByParents?.contains(CompletedByParent(callerUser.resolveUserId()))
                        ?: false
                isAssignedToParent && !parentHasCompleted
            }
            else -> {
                //Important for Child or Parent in name of Child
                val isAssignedToChild = c.assignedFor?.contains(AssignedFor.CHILD.type) ?: false
                val childHasCompleted = c.completed ?: false
                isAssignedToChild && !childHasCompleted
            }
        }
    }

    fun isCompleted(completion: ChallengeCompletion): Boolean {
        val assignedForChild = completion.assignedFor?.contains(AssignedFor.CHILD.type)
            ?: false
        val assignedForParent = completion.assignedFor?.contains(AssignedFor.PARENT.type)
            ?: false

        return if (assignedForChild && assignedForParent) {
            completion.completed == true && (completion.completedByParents?.isNotEmpty() ?: false)
        } else if (assignedForChild) {
            completion.completed == true
        } else if (assignedForParent) {
            completion.completedByParents?.isNotEmpty() ?: false
        } else {
            throw DataException("No body is assignedFor on ChallengeCompletion (${completion.id})")
        }
    }

    fun isCompletedByChild(completion: ChallengeCompletion): Boolean {
        val assignedForChild = completion.assignedFor?.contains(AssignedFor.CHILD.type)
            ?: throw DataException("No assignedFor on ChallengeCompletion (${completion.id})")
        val childHasCompleted = completion.completed ?: false
        return assignedForChild && childHasCompleted
    }

    fun isCompletedByParent(completion: ChallengeCompletion, parent: ObjectId): Boolean {
        val assignedForParent = completion.assignedFor?.contains(AssignedFor.PARENT.type)
            ?: throw DataException("No assignedFor on ChallengeCompletion (${completion.id})")
        val parentHasCompleted =
            completion.completedByParents?.contains(CompletedByParent(parent)) ?: false
        return assignedForParent && parentHasCompleted
    }

    /**
     * Challenge completion can get reward points if user completed:
     * - Periodic Challenge with amount requirement satisfied
     * - Periodic Challenge with no amount requirement to satisfy
     * - Non-periodic Challenge completed
     */
    fun canGetPoints(completion: ChallengeCompletion): Boolean {
        val periodDaysValue = completion.periodDays ?: 0
        val periodAmountValue = completion.periodAmount ?: 0
        val isPeriodicChallengeWithAmount = periodDaysValue > 0 && periodAmountValue > 0
        val isPeriodicChallengeWithoutAmount = periodDaysValue > 0 && periodAmountValue == 0
        val isNonPeriodicChallenge = periodDaysValue == 0L

        val programmingId = completion.programmingId
            ?: throw DataException("Completion has no programmingId (${completion.id})")
        val forId =
            completion.forId ?: throw DataException("Completion has no forId (${completion.id})")
        return if (isPeriodicChallengeWithAmount) {
            val completedCount = completionRepo.findByProgrammingIdAndForIdAndCompleted(
                programmingId,
                forId,
                true
            ).size
            completedCount == periodAmountValue
        } else {
            isNonPeriodicChallenge || isPeriodicChallengeWithoutAmount
        }
    }

    fun validateQuestionnaire(
        completionId: ObjectId,
        questionnaire: String,
        correctAnswerJson: String,
        userAnswerJson: String
    ): Boolean {
        val incorrectAnswers = mutableListOf<String>()

        val questionnaireMap: Map<*, *>
        val correctAnswerMap: Map<*, *>
        val userAnswerMap: Map<*, *>

        try {
            questionnaireMap = tools.jackson.readValue(questionnaire, Map::class.java)
        } catch (e: Exception) {
            throw DataException("Cannot parse stored JSON for questionnaire (ChallengeCompletion: $completionId)")
        }

        try {
            correctAnswerMap = tools.jackson.readValue(correctAnswerJson, Map::class.java)
        } catch (e: Exception) {
            throw DataException("Cannot parse stored JSON for correctAnswers (ChallengeCompletion: $completionId)")
        }

        try {
            userAnswerMap = tools.jackson.readValue(userAnswerJson, Map::class.java)
        } catch (e: Exception) {
            throw DataException("Cannot parse stored JSON for userAnswers (ChallengeCompletion: $completionId)")
        }

        val questions = try {
            ((questionnaireMap["pages"] as List<Map<String, List<Map<String, Any>>>>)
                .first()["elements"] as List<Map<String, Any>>)
        } catch (e: Exception) {
            throw DataException("Error while handling stored JSON (ChallengeCompletion: $completionId)")
        }

        for (question in questions) {
            val name = question["name"] as String
            val type = question["type"] as String
            val isRequiredAttr = question["isRequired"]
            val isRequired = if (isRequiredAttr is Boolean) {
                isRequiredAttr
            } else {
                isRequiredAttr == "true"
            }

            val correctAnswer = correctAnswerMap[name]
            val userAnswer = userAnswerMap[name]

            // Mark as incorrect if, current question is required but user hasn't answered
            if (isRequired && userAnswer == null) {
                incorrectAnswers.add(name)
                continue
            }

            // Avoid current question if, it isn't required and user hasn't answered
            if (!isRequired && userAnswer == null) {
                continue
            }

            // Mark as incorrect if stored correct answer values* DOESN'T MATCH user answer values*
            // (*values could be: List of values, unique String value, etc)
            // If 'text' type question we cant validate content
            if (type != "text" && correctAnswer != null && correctAnswer != userAnswer) {
                incorrectAnswers.add(name)
                continue
            }
        }


        if (incorrectAnswers.isNotEmpty()) {
            //We return back to frontend, which are the incorrect answers. Frontend should remark them as incorrect.
            throw BadRequestException(tools.jackson.writeValueAsString(incorrectAnswers))
        }

        return true
    }
}