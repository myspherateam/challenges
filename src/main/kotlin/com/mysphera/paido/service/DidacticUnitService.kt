package com.mysphera.paido.service

import com.mysphera.paido.BadRequestException
import com.mysphera.paido.ServerException
import com.mysphera.paido.data.ChallengeDefinitionRepository
import com.mysphera.paido.data.DidacticUnit
import com.mysphera.paido.data.DidacticUnitRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DidacticUnitService {
    @Autowired
    private lateinit var repository: DidacticUnitRepository

    @Autowired
    private lateinit var challengeDefinitionRepository: ChallengeDefinitionRepository

    private fun validate(unit: DidacticUnit) {
        unit.name?.let {
            if (it.trim().isEmpty()) {
                throw BadRequestException("name was not valid")
            }
        } ?: throw BadRequestException("name was not provided")

        val challenges = unit.challenges ?: throw BadRequestException("challenges was not provided")
        if (challenges.isEmpty()) throw BadRequestException("challenges must not be empty")

        val maxEndTime = challenges.maxBy { it.endOffset }?.endOffset
            ?: throw BadRequestException("challenges must not be empty")
        unit.duration = maxEndTime
        val minStartTime = challenges.minBy { it.startOffset }?.startOffset
            ?: throw BadRequestException("challenges must not be empty")
        if (minStartTime != 0L) throw throw BadRequestException("at least one challenge must start when the unit starts (at 0)")
    }

    fun save(unit: DidacticUnit): DidacticUnit {
        validate(unit)
        val toReturn = repository.save(unit)
        editChallenges(toReturn)
        return toReturn
    }

    fun edit(unit: DidacticUnit): DidacticUnit {
        val unitId = unit.id ?: throw BadRequestException("id is required to edit")
        val storedUnit = repository.findById(unitId).orElseThrow {
            throw BadRequestException("The id provided doesn't exists")
        }
        removeChallenges(storedUnit, unit)
        storedUnit.name = unit.name ?: storedUnit.name
        storedUnit.duration = unit.duration ?: storedUnit.duration
        storedUnit.challenges = unit.challenges ?: storedUnit.challenges
        storedUnit.badgeId = unit.badgeId ?: storedUnit.badgeId
        validate(storedUnit)
        val toReturn = repository.save(storedUnit)
        editChallenges(toReturn)
        return toReturn
    }

    fun editChallenges(unit: DidacticUnit) {
        val unitId = unit.id ?: throw ServerException("DidacticUnit's id not set after saving")
        val challengeIds = unit.challenges?.map { it.challengeId } ?: listOf()
        challengeDefinitionRepository.addDidacticUnit(unitId, challengeIds)
    }

    fun removeChallenges(oldUnit: DidacticUnit, newUnit: DidacticUnit) {
        val unitId = oldUnit.id ?: throw ServerException("DidacticUnit's id not set after saving")
        val oldChallenges = oldUnit.challenges?.map { it.challengeId } ?: listOf()
        val newChallenges = newUnit.challenges?.map { it.challengeId } ?: listOf()
        val removedChallenges = oldChallenges.minus(newChallenges.toSet())
        challengeDefinitionRepository.removeDidacticUnit(unitId, removedChallenges)
    }
}