package com.mysphera.paido

import com.fasterxml.jackson.databind.ObjectMapper
import org.bson.types.ObjectId
import org.locationtech.jts.geom.Coordinate
import org.locationtech.jts.geom.GeometryFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.PageRequest
import org.springframework.http.*
import org.springframework.stereotype.Component
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate
import java.security.SecureRandom
import java.util.*
import kotlin.math.max

object FullRandomString :
    RandomString(characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")

open class RandomString(characters: String) {
    private val random: Random
    private val symbols: CharArray
    private var buf: CharArray = CharArray(0);

    init {
        random = SecureRandom()
        symbols = characters.toCharArray()
    }

    /**
     * Generate a random string.
     */
    fun nextString(length: Int): String {
        if (length < 1) throw IllegalArgumentException()
        this.buf = CharArray(length)
        for (idx in buf.indices) {
            buf[idx] = symbols[random.nextInt(symbols.size)]
        }
        return String(buf)
    }
}

@Component
class MicroServicesToolbox(
    @Value("\${microservice.administration.base-endpoint}")
    var administrationEndpoint: String,
    @Value("\${oauth.login.url}")
    var authLoginURL: String,
    @Value("\${oauth.login.username}")
    var authUsername: String,
    @Value("\${oauth.login.password}")
    var authPassword: String,
    @Value("\${oauth.login.clientId}")
    var authClientId: String,
    @Value("\${oauth.login.grantType}")
    var authGrantType: String,

    @Autowired
    var http: RestTemplate,
    @Autowired
    var jackson: ObjectMapper
) {
    fun doLogin(): Map<*, *> {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_FORM_URLENCODED

        val request = LinkedMultiValueMap<String, String>()
        request["username"] = authUsername
        request["password"] = authPassword
        request["client_id"] = authClientId
        request["grant_type"] = authGrantType

        val entity = HttpEntity(request, headers)

        val response = this.http.exchange(authLoginURL, HttpMethod.POST, entity, Map::class.java)
        return when (response.statusCodeValue) {
            200 -> {
                response.body ?: throw ServerException("Error with OAuth response")
            }
            else -> {
                throw ServerException("Error with OAuth server $response")
            }
        }
    }

    fun findUser(token: String, id: ObjectId, childId: String?): User {
        val parameter = if (childId != null) "?childId=$childId" else ""
        val response = this.http.doGET(token, "$administrationEndpoint/users/$id${parameter}")
        return when (response.statusCodeValue) {
            HttpStatus.OK.value() -> {
                val body = response.body as Response<*>
                jackson.convertValue(body.message, User::class.java)
            }
            HttpStatus.NOT_FOUND.value() -> {
                throw DataException("No user was found")
            }
            HttpStatus.FORBIDDEN.value() -> {
                val body = response.body as Response<*>
                throw ForbiddenException(body.message)
            }
            else -> {
                throw ServerException(response)
            }
        }
    }

    fun findUsers(
        token: String,
        users: List<ObjectId>? = null,
        groups: List<ObjectId>? = null,
        entities: List<ObjectId>? = null,
        roles: List<String>? = null,
        pageRequest: PageRequest? = null
    ): List<User> {
        val queryParamList = mutableListOf<String>()

        if (!users.isNullOrEmpty()) {
            queryParamList.add("ids=${users.joinToString(",")}")
        }
        if (!groups.isNullOrEmpty()) {
            queryParamList.add("groupIds=${groups.joinToString(",")}")
        }
        if (!entities.isNullOrEmpty()) {
            queryParamList.add("entityIds=${entities.joinToString(",")}")
        }
        if (!roles.isNullOrEmpty()) {
            queryParamList.add("roles=${roles.joinToString(",")}")
        }

        if (pageRequest != null) {
            queryParamList.add("page=${pageRequest.pageNumber}")
            queryParamList.add("pageSize=${pageRequest.pageSize}")
        }

        val requestUrl = if (queryParamList.isEmpty()) {
            "$administrationEndpoint/users"
        } else {
            "$administrationEndpoint/users?${queryParamList.joinToString("&")}"
        }

        val response = http.doGET(token, requestUrl)

        when (response.statusCodeValue) {
            HttpStatus.OK.value() -> {
                val body = response.body as Response<*>
                val data = jackson.convertValue(body.message, List::class.java)
                return data.map { userData ->
                    jackson.convertValue(userData, User::class.java)
                }
            }
            HttpStatus.FORBIDDEN.value() -> {
                val body = response.body as Response<*>
                throw ForbiddenException(body.message)
            }
            else -> {
                throw ServerException("Administration backend failing... (HTTP Error $response)")
            }
        }
    }
}

fun clamp(min: Int, value: Int, max: Int): Int {
    return if (value < min) {
        min
    } else {
        if (value > max) max else value
    }
}

fun getPageRequest(pageNumber: Int?, pageSize: Int?): PageRequest {
    val minPage = 0
    val maxSize = 1000

    return if (pageNumber == null || pageSize == null) {
        PageRequest.of(minPage, maxSize)
    } else {
        PageRequest.of(max(pageNumber, minPage), clamp(1, pageSize, maxSize))
    }
}

fun isPointInsidePolygon(point: Array<Double>, polygon: Array<Array<Double>>): Boolean {
    if (point.isEmpty() || polygon.isEmpty()) {
        return false
    }

    val factory = GeometryFactory()

    val polygonCoordinates = mutableListOf<Coordinate>()
    polygon.forEach {
        val x = it[0]
        val y = it[1]
        polygonCoordinates.add(Coordinate(x, y))
    }

    val pointX = point[0]
    val pointY = point[1]
    val pointCoordinate = Coordinate(pointX, pointY)

    val polygonGeo = factory.createPolygon(polygonCoordinates.toTypedArray())
    val pointGeo = factory.createPoint(pointCoordinate)

    return polygonGeo.contains(pointGeo)
}