package com.mysphera.paido

import mu.KotlinLogging
import org.bson.types.ObjectId
import org.jose4j.jwk.HttpsJwks
import org.jose4j.jwt.consumer.InvalidJwtException
import org.jose4j.jwt.consumer.JwtConsumer
import org.jose4j.jwt.consumer.JwtConsumerBuilder
import org.jose4j.keys.resolvers.HttpsJwksVerificationKeyResolver
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct
import javax.servlet.http.HttpServletRequest

@Component
class PAIDOAuthenticator {
    companion object {
        private const val CARING_FOR_FIELD = "caring_for"
        private const val INFO_FIELD = "info"
        private const val ID_FIELD = "_id"
    }

    @Suppress("unused")
    private val logger = KotlinLogging.logger { }

    @Value("\${oauth.jwt-validation.url}")
    private lateinit var jwtValidationURL: String

    private lateinit var httpsJkws: HttpsJwks
    private lateinit var httpsJwksKeyResolver: HttpsJwksVerificationKeyResolver
    private lateinit var jwtConsumer: JwtConsumer

    @Suppress("unused")
    @PostConstruct()
    fun init() {
        httpsJkws = HttpsJwks(jwtValidationURL)
        httpsJwksKeyResolver = HttpsJwksVerificationKeyResolver(httpsJkws)
        jwtConsumer = JwtConsumerBuilder()
            .setVerificationKeyResolver(httpsJwksKeyResolver)
            .setExpectedAudience("account")
            .build()
    }

    fun requestToPayload(request: HttpServletRequest): JWTPayload {
        val token = request.getHeader(HttpHeaders.AUTHORIZATION)?.substring("Bearer ".length)
            ?: throw MissingAuthenticationException(request.headerNames.toList().toString())
        val childId: ObjectId? = request.getParameter("childId")?.let { ObjectId(it) }
        try {
            val res = jwtConsumer.process(token)

            val info = res.jwtClaims.getClaimValue(INFO_FIELD) as Map<*, *>
            val id = (info[ID_FIELD] as? String)?.let { ObjectId(it) }
                ?: throw MissingAuthenticationException()
            val caringFor =
                info[CARING_FOR_FIELD] as? List<*> ?: throw MissingAuthenticationException()
            val caringForIds = caringFor.map {
                val oid = it as String
                ObjectId(oid)
            }
            if (childId != null && !caringForIds.contains(childId)) {
                throw ForbiddenException(
                    "$childId is not in the list of children the user cares for [${
                        caringForIds.joinToString(
                            ", "
                        ) { it.toString() }
                    }]"
                )
            }

            return JWTPayload(childId ?: id, id, caringForIds)
        } catch (exception: InvalidJwtException) {
            //JWTVerificationException is thrown if the token is not valid (either someone tampered with it or it expired)
            throw MissingAuthenticationException(exception.message) // Return 401 Unauthorized
        }
    }
}