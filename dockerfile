# syntax = docker/dockerfile:1.2
from gradle:5.5.0-jdk8 as BUILDER
WORKDIR /home/gradle/src
#COPY --chown=gradle:gradle build.gradle settings.gradle gradle.properties ./
#RUN gradle dependencies
COPY --chown=gradle:gradle . .
#run gradle generateSources bootjar
#run --mount=type=cache,target=/home/gradle/.gradle gradle bootjar
run gradle bootjar

FROM openjdk:8-jre-alpine
EXPOSE 8080
ENV SPRING_PROFILES_ACTIVE="prod"
COPY --from=BUILDER /home/gradle/src/build/libs/challenges*.jar app.jar
ENTRYPOINT java -jar /app.jar